-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3307
-- Generation Time: Jul 13, 2020 at 12:41 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `wedding`
--

-- --------------------------------------------------------

--
-- Table structure for table `cover`
--

CREATE TABLE `cover` (
  `id` int(11) NOT NULL,
  `logo` varchar(200) NOT NULL,
  `background` varchar(200) NOT NULL,
  `is_bg` int(11) NOT NULL DEFAULT '0',
  `frame` varchar(200) NOT NULL,
  `frame_1` int(11) NOT NULL DEFAULT '0',
  `frame_2` int(11) NOT NULL DEFAULT '0',
  `frame_3` int(11) NOT NULL DEFAULT '0',
  `frame_4` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cover`
--

INSERT INTO `cover` (`id`, `logo`, `background`, `is_bg`, `frame`, `frame_1`, `frame_2`, `frame_3`, `frame_4`) VALUES
(1, '1674.png', 'beautiful-background-roses-valentine-s-day_24972-167.jpg', 0, 'topleft.png', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `data_nikah`
--

CREATE TABLE `data_nikah` (
  `id` int(11) NOT NULL,
  `mempelai_pria` varchar(200) NOT NULL,
  `mempelai_wanita` varchar(200) NOT NULL,
  `putra_ke` varchar(50) NOT NULL,
  `ayah_pria` varchar(200) NOT NULL,
  `ibu_pria` varchar(200) NOT NULL,
  `putri_ke` varchar(50) NOT NULL,
  `ayah_wanita` varchar(200) NOT NULL,
  `ibu_wanita` varchar(200) NOT NULL,
  `tanggal_akad` datetime NOT NULL,
  `tempat_akad` varchar(200) NOT NULL,
  `alamat_akad` text NOT NULL,
  `tanggal_resepsi` datetime NOT NULL,
  `tempat_resepsi` varchar(200) NOT NULL,
  `alamat_resepsi` text NOT NULL,
  `url_map` varchar(400) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_nikah`
--

INSERT INTO `data_nikah` (`id`, `mempelai_pria`, `mempelai_wanita`, `putra_ke`, `ayah_pria`, `ibu_pria`, `putri_ke`, `ayah_wanita`, `ibu_wanita`, `tanggal_akad`, `tempat_akad`, `alamat_akad`, `tanggal_resepsi`, `tempat_resepsi`, `alamat_resepsi`, `url_map`) VALUES
(1, 'Sandi', 'Ulfa', 'Kedua', 'Bpk. Fulan Pria', 'Ibu Fulan Pria', 'Ketiga', 'Bpk. Fulan Wanita', 'Ibu Fulan Wanita', '2020-03-31 09:00:00', 'Desa Lorem', 'Jl. Desa Lorem Sebelah Ipsum RT 001/ RW 002', '2020-04-02 09:00:00', 'Desa Ipsum', 'Jl. Desa IpsumSebelah Lorem RT 001/ RW 002', 'https://goo.gl/maps/pQhwRuvSXmQVe8ty5');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `slide_eight`
--

CREATE TABLE `slide_eight` (
  `id` int(11) NOT NULL,
  `background` varchar(200) NOT NULL,
  `is_bg` int(11) NOT NULL DEFAULT '0',
  `frame` varchar(200) NOT NULL,
  `frame_1` int(11) NOT NULL DEFAULT '0',
  `frame_2` int(11) NOT NULL DEFAULT '0',
  `frame_3` int(11) NOT NULL DEFAULT '0',
  `frame_4` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slide_eight`
--

INSERT INTO `slide_eight` (`id`, `background`, `is_bg`, `frame`, `frame_1`, `frame_2`, `frame_3`, `frame_4`) VALUES
(1, 'beautiful-background-roses-valentine-s-day_24972-167.jpg', 0, 'topleft.png', 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `slide_five`
--

CREATE TABLE `slide_five` (
  `id` int(11) NOT NULL,
  `background` varchar(200) NOT NULL,
  `is_bg` int(11) NOT NULL DEFAULT '0',
  `frame` varchar(200) NOT NULL,
  `frame_1` int(11) NOT NULL DEFAULT '0',
  `frame_2` int(11) NOT NULL DEFAULT '0',
  `frame_3` int(11) NOT NULL DEFAULT '0',
  `frame_4` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slide_five`
--

INSERT INTO `slide_five` (`id`, `background`, `is_bg`, `frame`, `frame_1`, `frame_2`, `frame_3`, `frame_4`) VALUES
(1, 'beautiful-background-roses-valentine-s-day_24972-167.jpg', 0, 'topleft.png', 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `slide_four`
--

CREATE TABLE `slide_four` (
  `id` int(11) NOT NULL,
  `background` varchar(200) NOT NULL,
  `is_bg` int(11) NOT NULL DEFAULT '0',
  `frame` varchar(200) NOT NULL,
  `frame_1` int(11) NOT NULL DEFAULT '0',
  `frame_2` int(11) NOT NULL DEFAULT '0',
  `frame_3` int(11) NOT NULL DEFAULT '0',
  `frame_4` int(11) NOT NULL DEFAULT '0',
  `gambar_lokasi` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slide_four`
--

INSERT INTO `slide_four` (`id`, `background`, `is_bg`, `frame`, `frame_1`, `frame_2`, `frame_3`, `frame_4`, `gambar_lokasi`) VALUES
(1, 'beautiful-background-roses-valentine-s-day_24972-167.jpg', 0, 'topleft.png', 0, 0, 1, 0, 'oke.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `slide_one`
--

CREATE TABLE `slide_one` (
  `id` int(11) NOT NULL,
  `logo` varchar(200) NOT NULL,
  `foto_mempelai` varchar(200) NOT NULL,
  `background` varchar(200) NOT NULL,
  `is_bg` int(3) NOT NULL DEFAULT '0',
  `frame` varchar(200) NOT NULL,
  `frame_1` int(3) NOT NULL DEFAULT '0',
  `frame_2` int(3) NOT NULL DEFAULT '0',
  `frame_3` int(3) NOT NULL DEFAULT '0',
  `frame_4` int(3) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slide_one`
--

INSERT INTO `slide_one` (`id`, `logo`, `foto_mempelai`, `background`, `is_bg`, `frame`, `frame_1`, `frame_2`, `frame_3`, `frame_4`) VALUES
(1, '1674.png', 'image1.png', 'beautiful-background-roses-valentine-s-day_24972-167.jpg', 0, 'topleft.png', 1, 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `slide_seven`
--

CREATE TABLE `slide_seven` (
  `id` int(11) NOT NULL,
  `background` varchar(200) NOT NULL,
  `is_bg` int(11) NOT NULL DEFAULT '0',
  `frame` varchar(200) NOT NULL,
  `frame_2` int(11) NOT NULL DEFAULT '0',
  `frame_4` int(11) NOT NULL DEFAULT '0',
  `bg_main` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slide_seven`
--

INSERT INTO `slide_seven` (`id`, `background`, `is_bg`, `frame`, `frame_2`, `frame_4`, `bg_main`) VALUES
(1, 'beautiful-background-roses-valentine-s-day_24972-167.jpg', 0, 'topleft.png', 1, 0, 'wedding-moments-newly-wed-couple-s-hands-with-wedding-rings_8353-5792.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `slide_six`
--

CREATE TABLE `slide_six` (
  `id` int(11) NOT NULL,
  `file` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slide_six`
--

INSERT INTO `slide_six` (`id`, `file`) VALUES
(1, 'beautiful-brides-are-photographed-near-old-house_8353-9507.jpg'),
(2, 'bride-groom-posing-cliff-blue-sky-sea_158538-8337.jpg'),
(3, 'groom-black-tuxedo-hugs-tender-stunning-bride-while-they-stand_8353-8050.jpg'),
(4, ''),
(5, '');

-- --------------------------------------------------------

--
-- Table structure for table `slide_three`
--

CREATE TABLE `slide_three` (
  `id` int(11) NOT NULL,
  `content` varchar(200) NOT NULL,
  `background` varchar(200) NOT NULL,
  `is_bg` int(11) NOT NULL DEFAULT '0',
  `frame` varchar(200) NOT NULL,
  `frame_1` int(11) NOT NULL DEFAULT '0',
  `frame_2` int(11) NOT NULL DEFAULT '0',
  `frame_3` int(11) NOT NULL DEFAULT '0',
  `frame_4` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slide_three`
--

INSERT INTO `slide_three` (`id`, `content`, `background`, `is_bg`, `frame`, `frame_1`, `frame_2`, `frame_3`, `frame_4`) VALUES
(1, 'beautiful-background-roses-valentine-s-day_24972-167.jpg', 'beautiful-background-roses-valentine-s-day_24972-167.jpg', 0, 'topleft.png', 1, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `slide_two`
--

CREATE TABLE `slide_two` (
  `id` int(11) NOT NULL,
  `foto_mempelai_pria` varchar(200) NOT NULL,
  `foto_mempelai_wanita` varchar(200) NOT NULL,
  `background` varchar(200) NOT NULL,
  `is_bg` int(3) NOT NULL DEFAULT '0',
  `frame` varchar(200) NOT NULL,
  `frame_1` int(11) NOT NULL DEFAULT '0',
  `frame_2` int(11) NOT NULL DEFAULT '0',
  `frame_3` int(11) NOT NULL DEFAULT '0',
  `frame_4` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slide_two`
--

INSERT INTO `slide_two` (`id`, `foto_mempelai_pria`, `foto_mempelai_wanita`, `background`, `is_bg`, `frame`, `frame_1`, `frame_2`, `frame_3`, `frame_4`) VALUES
(1, 'person1.jpg', 'person2.jpg', 'beautiful-background-roses-valentine-s-day_24972-167.jpg', 0, 'topleft.png', 0, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tamu_undangan`
--

CREATE TABLE `tamu_undangan` (
  `id` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tamu_undangan`
--

INSERT INTO `tamu_undangan` (`id`, `nama`) VALUES
(37, 'Bpk. RW aja'),
(38, 'tes');

-- --------------------------------------------------------

--
-- Table structure for table `ucapan`
--

CREATE TABLE `ucapan` (
  `id` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `ucapan` text NOT NULL,
  `kehadiran` varchar(20) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1594630997, 1, 'Admin', 'istrator', 'ADMIN', '0');

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cover`
--
ALTER TABLE `cover`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `data_nikah`
--
ALTER TABLE `data_nikah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slide_eight`
--
ALTER TABLE `slide_eight`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slide_five`
--
ALTER TABLE `slide_five`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slide_four`
--
ALTER TABLE `slide_four`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slide_one`
--
ALTER TABLE `slide_one`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slide_seven`
--
ALTER TABLE `slide_seven`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slide_six`
--
ALTER TABLE `slide_six`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slide_three`
--
ALTER TABLE `slide_three`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slide_two`
--
ALTER TABLE `slide_two`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tamu_undangan`
--
ALTER TABLE `tamu_undangan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ucapan`
--
ALTER TABLE `ucapan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_nikah`
--
ALTER TABLE `data_nikah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `slide_one`
--
ALTER TABLE `slide_one`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `slide_six`
--
ALTER TABLE `slide_six`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `slide_three`
--
ALTER TABLE `slide_three`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `slide_two`
--
ALTER TABLE `slide_two`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tamu_undangan`
--
ALTER TABLE `tamu_undangan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `ucapan`
--
ALTER TABLE `ucapan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
