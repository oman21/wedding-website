<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MX_Controller {
	function __construct() {
        parent::__construct();
    }

	public function index()
	{
		$data = $this->get_data('');
		$this->load->view('welcome_message', $data);
	}

	public function invitation($data)
	{
		$explode = explode('-', $data);
		$invitation = array(
			'nama' => $explode[0]?str_replace('+', ' ', $explode[0]):'',
			'jabatan' => $explode[1]?str_replace('+', ' ', $explode[1]):'',
			'alamat' => $explode[2]?str_replace('+', ' ', $explode[2]):''
		);

		$data = $this->get_data($invitation);
		$this->load->view('welcome_message', $data);
	}

	public function ucapan(){
		$data = array(
			'nama' => $this->input->post('nama'),
			'ucapan' => $this->input->post('ucapan'),
			'kehadiran' => $this->input->post('kehadiran'),
		);

		$result = $this->db->insert('ucapan', $data);
		if($result){
	    	echo json_encode(array("status" => TRUE));
	    }else{
	    	echo json_encode(array("status" => FALSE));
	    }

	}

	private function date_name($hari)
	{
		switch($hari){
			case 'Sun':
				$hari_ini = "Minggu";
			break;
	 
			case 'Mon':			
				$hari_ini = "Senin";
			break;
	 
			case 'Tue':
				$hari_ini = "Selasa";
			break;
	 
			case 'Wed':
				$hari_ini = "Rabu";
			break;
	 
			case 'Thu':
				$hari_ini = "Kamis";
			break;
	 
			case 'Fri':
				$hari_ini = "Jumat";
			break;
	 
			case 'Sat':
				$hari_ini = "Sabtu";
			break;
			
			default:
				$hari_ini = "Tidak di ketahui";		
			break;
		}
	 
		return $hari_ini;
	}

	private function tgl_indo($tanggal){
		$bulan = array (
			1 =>   'Januari',
					'Februari',
					'Maret',
					'April',
					'Mei',
					'Juni',
					'Juli',
					'Agustus',
					'September',
					'Oktober',
					'November',
					'Desember'
		);
		$pecahkan = explode('-', $tanggal);

		return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
	}

	private function get_data($invitation){
		$this->db->where('id', 1);
		$data_nikah = $this->db->get('data_nikah')->row();

		$tamu_undangan = $this->db->get('tamu_undangan')->result();

		$date_name_resepsi = $this->date_name(date_format(date_create($data_nikah->tanggal_resepsi), 'D'));
		$date_format_resepsi = $this->tgl_indo(date_format(date_create($data_nikah->tanggal_resepsi), 'Y-m-d'));

		$date_name_akad = $this->date_name(date_format(date_create($data_nikah->tanggal_akad), 'D'));
		$date_format_akad = $this->tgl_indo(date_format(date_create($data_nikah->tanggal_akad), 'Y-m-d'));

		$this->db->where('id', 1);
		$slide_one = $this->db->get('slide_one')->row();

		$this->db->where('id', 1);
		$slide_two = $this->db->get('slide_two')->row();

		$this->db->where('id', 1);
		$slide_three = $this->db->get('slide_three')->row();

		$this->db->where('id', 1);
		$slide_four = $this->db->get('slide_four')->row();

		$this->db->where('id', 1);
		$slide_five = $this->db->get('slide_five')->row();

		$this->db->where('file != ""');
		$slide_six = $this->db->get('slide_six')->result();

		$this->db->where('id', 1);
		$slide_seven = $this->db->get('slide_seven')->row();

		$this->db->where('id', 1);
		$slide_eight = $this->db->get('slide_eight')->row();

		$this->db->where('id', 1);
		$cover = $this->db->get('cover')->row();

		$data = array(
			'data_nikah' => $data_nikah,
			'hari_resepsi' => $date_name_resepsi,
			'tanggal_resepsi' => $date_format_resepsi,
			'hari_akad' => $date_name_akad,
			'tanggal_akad' => $date_format_akad,
			'tamu_undangan' => $tamu_undangan,
			'invitation' => $invitation,
			'slide_one' => $slide_one,
			'slide_two' => $slide_two,
			'slide_three' => $slide_three,
			'slide_four' => $slide_four,
			'slide_five' => $slide_five,
			'slide_six' => $slide_six,
			'slide_seven' => $slide_seven,
			'slide_eight' => $slide_eight,
			'cover' => $cover
		);

		return $data;
	}
}
