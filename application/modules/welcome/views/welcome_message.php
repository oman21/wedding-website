<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>Wedding</title>
  <meta name="title" content="" />
  <meta name="description" content="" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

  <link href='<?=base_url()?>assets/css/onepage-scroll.css' rel='stylesheet' type='text/css'>

  <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/js/jquery.onepage-scroll.js"></script>

  <!-- fontawesome -->
  <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- bootstrap -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

  <!-- custom -->
  <link href='<?=base_url()?>assets/css/style.css' rel='stylesheet' type='text/css'>

</head>
<body>
  <div class="opening" id="open-button" style="background-image: url('<?=$cover->is_bg==1?base_url()."assets/image/".$cover->background:""?>');">
    
    <?php
      if($cover->frame_1==1){
    ?>
      <img src="<?=base_url()?>assets/image/<?=$cover->frame?>" class="topleft">
    <?php
      }
    ?>

    <?php
      if($cover->frame_2==1){
    ?>
      <img src="<?=base_url()?>assets/image/<?=$cover->frame?>" class="topright">
    <?php
      }
    ?>
    
    <?php
      if($cover->frame_3==1){
    ?>
      <img src="<?=base_url()?>assets/image/<?=$cover->frame?>" class="bottomleft">
    <?php
      }
    ?>

    <?php
      if($cover->frame_4==1){
    ?>
      <img src="<?=base_url()?>assets/image/<?=$cover->frame?>" class="bottomright">
    <?php
      }
    ?>

    <div class="opening-box">

      <img src="<?=$cover->logo!=''?base_url().'assets/image/'.$cover->logo:''?>" class="logo">
      <?php
        if($invitation!=''){
      ?>
        <p class="yth">Kepada Yth:</p>
        <p class="name"><?=$invitation['nama']?></p>
        <p class="name"><?=$invitation['jabatan']?></p>
        <p class="yth" style="margin-bottom:0">di</p>
        <p class="yth"><?=$invitation['alamat']?></p>
      <?php
        }
      ?>
      <br/>
      <p class="shimmer open-text">Klik Layar Untuk<br>Membuka Undangan</p>
    </div>
  </div>
  <div class="wrapper">
    <div class="main">
      <section class="page1" style="background-image: url('<?=$slide_one->is_bg==1?base_url()."assets/image/".$slide_one->background:""?>');">
        <?php
          if($slide_one->frame_1==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_one->frame?>" class="topleft">
        <?php
          }
        ?>

        <?php
          if($slide_one->frame_2==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_one->frame?>" class="topright">
        <?php
          }
        ?>
        
        <?php
          if($slide_one->frame_3==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_one->frame?>" class="bottomleft">
        <?php
          }
        ?>

        <?php
          if($slide_one->frame_4==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_one->frame?>" class="bottomright">
        <?php
          }
        ?>

        <img src="<?=$slide_one->foto_mempelai==''?'':base_url().'assets/image/'.$slide_one->foto_mempelai?>" class="image1" />

        <div class="savedate">
          <img src="<?=$slide_one->logo==''?'':base_url().'assets/image/'.$slide_one->logo?>" class="logo">
          <p class="save">Save Our Date</p>
          <p class="date"><?=$hari_resepsi?>, <?=$tanggal_resepsi?></p>
          <br/>
          <!-- <a class="button" href="http://www.google.com/calendar/event?action=TEMPLATE&dates=20180821%2F20180822&text=Litmus%20Live%20London&location=etc.venues%20155%20Bishopsgate&details=Litmus%20Live%20helps%20you%20become%20a%20better%20email%20marketer%20with%20real-world%20advice%2C%20best%20practices%20%26%20practical%20takeaways." class="cta gmail-show">Save the date</a> -->
        </div>
        <div class="geser">
          <i class="shimmer fa fa-arrow-up"></i>
          <br/>
          <p class="shimmer">Geser Ke Atas</p>
        </div>
      </section>
      
      <section class="page2" style="background-image: url('<?=$slide_two->is_bg==1?base_url()."assets/image/".$slide_two->background:""?>');">
        <?php
          if($slide_two->frame_1==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_two->frame?>" class="topleft">
        <?php
          }
        ?>

        <?php
          if($slide_two->frame_2==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_two->frame?>" class="topright">
        <?php
          }
        ?>
        
        <?php
          if($slide_two->frame_3==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_two->frame?>" class="bottomleft">
        <?php
          }
        ?>

        <?php
          if($slide_two->frame_4==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_two->frame?>" class="bottomright">
        <?php
          }
        ?>

        <div class="box-wrap center">
          <div class="box-wrap-justify">
            <img src="https://undangan.simanten.com/images/librari/assalamualaikum_1.png" class="bismillah">
            <br/>
            <i>Assalamu'alaikum warahmatullahi wabarakatuh</i>
            <br/>
            <br/>
            <p class="doa">Dengan memohon rahmat dan ridho Allah SWT, Kami akan menyelenggarakan resepsi pernikahan Putra-Putri kami</p>
            <div class="row img-ppl-wrap">
              <div class="offset-md-2 col-md-3">
                <div class="image-circle">
                  <img src="<?=$slide_two->foto_mempelai_pria==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_two->foto_mempelai_pria?>" width="100%">
                </div>
                <div class="name-married-wrap">
                  <p class="name-married"><?=$data_nikah->mempelai_pria?></p>
                  <img src="https://undangan.simanten.com/images/tema/tema01/line.png"/>
                  <p class="anak-text">Putra <?=$data_nikah->putra_ke?></p>
                  <p class="parent-text"><?=$data_nikah->ayah_pria?> & <?=$data_nikah->ibu_pria?></p>
                </div>
              </div>
              <div class="col-md-2">
                <p class="and-text">&</p>
              </div>
              <div class="col-md-3">
                <div class="image-circle">
                  <img src="<?=$slide_two->foto_mempelai_wanita==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_two->foto_mempelai_wanita?>" width="100%">
                </div>
                <div class="name-married-wrap">
                  <p class="name-married"><?=$data_nikah->mempelai_wanita?></p>
                  <img src="https://undangan.simanten.com/images/tema/tema01/line.png"/>
                  <p class="anak-text">Putri <?=$data_nikah->putri_ke?></p>
                  <p class="parent-text"><?=$data_nikah->ayah_wanita?> & <?=$data_nikah->ibu_wanita?></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      
      <section class="page3" style="background-image: url('<?=$slide_three->is_bg==1?base_url()."assets/image/".$slide_three->background:""?>');">
        <?php
          if($slide_three->frame_1==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_three->frame?>" class="topleft">
        <?php
          }
        ?>

        <?php
          if($slide_three->frame_2==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_three->frame?>" class="topright">
        <?php
          }
        ?>
        
        <?php
          if($slide_three->frame_3==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_three->frame?>" class="bottomleft">
        <?php
          }
        ?>

        <?php
          if($slide_three->frame_4==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_three->frame?>" class="bottomright">
        <?php
          }
        ?>

        <div class="jadwal">
          <h3>RANGKAIAN ACARA</h3>
          <div class="row">
            <div class="col-md-6">
              <h5>Akad Nikah</h5>
              <div class="row">
                <div class="col-md-4 col-xs-4 col-sm-4 calendar-date-container">
                  <div class="calendar-date">
                    <div class="calendar-date-wrap">
                      <?php
                        $explode_akad = explode(' ', $tanggal_akad);
                      ?>
                      <p><?=$explode_akad[0]?></p>
                      <p class="month"><?=$explode_akad[1]?></p>
                      <p class="year"><?=$explode_akad[2]?></p>
                    </div>
                  </div>
                </div>
                <div class="col-md-8 col-xs-8 col-sm-8 date-time">
                  <b><?=$hari_akad?></b>
                  <p><?=date_format(date_create($data_nikah->tanggal_akad), 'H:i')?> - Selesai</p>
                  <b class="desa"><?=$data_nikah->tempat_akad?></b>
                  <p class="alamat"><?=$data_nikah->alamat_akad?></p>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <h5>Resepsi</h5>
              <div class="row">
                <div class="col-md-4 calendar-date-container">
                  <div class="calendar-date">
                    <div class="calendar-date-wrap">
                      <?php
                        $explode_resepsi = explode(' ', $tanggal_resepsi);
                      ?>
                      <p><?=$explode_resepsi[0]?></p>
                      <p class="month"><?=$explode_resepsi[1]?></p>
                      <p class="year"><?=$explode_resepsi[2]?></p>
                    </div>
                  </div>
                </div>
                <div class="col-md-8 date-time">
                  <b><?=$hari_resepsi?></b>
                  <p><?=date_format(date_create($data_nikah->tanggal_resepsi), 'H:i')?> - Selesai</p>
                  <b class="desa"><?=$data_nikah->tempat_resepsi?></b>
                  <p class="alamat"><?=$data_nikah->alamat_resepsi?></p>
                </div>
              </div>
            </div>
          </div>

          <div class="countdown">
            <h5>Hitung Mundur Acara <b>Resepsi</b></h5>
            <div id="clockdiv" data-date="<?=$data_nikah->tanggal_resepsi?>">
              <div>
                <span class="days"></span>
                <div class="smalltext">Hari</div>
              </div>
              <div>
                <span class="hours"></span>
                <div class="smalltext">Jam</div>
              </div>
              <div>
                <span class="minutes"></span>
                <div class="smalltext">Menit</div>
              </div>
              <div>
                <span class="seconds"></span>
                <div class="smalltext">Detik</div>
              </div>
              <br>
            </div>

          </div>
        </div>
        
        <div class="section2">
          <div class="contex">
            <div class="line"></div>
            <p>Save The Date</p>
          </div>
        </div>

        <div class="flower" style="background-image: url('<?=$slide_three->content==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_three->content?>');"></div>
      </section>

      <section class="page4" style="background-image: url('<?=$slide_four->is_bg==1?base_url()."assets/image/".$slide_four->background:""?>');">
        <?php
          if($slide_four->frame_1==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_four->frame?>" class="topleft">
        <?php
          }
        ?>

        <?php
          if($slide_four->frame_2==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_four->frame?>" class="topright">
        <?php
          }
        ?>
        
        <?php
          if($slide_four->frame_3==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_four->frame?>" class="bottomleft">
        <?php
          }
        ?>

        <?php
          if($slide_four->frame_4==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_four->frame?>" class="bottomright">
        <?php
          }
        ?>

        <div class="page4-wrap" style="background-image: url('<?=base_url()?>assets/image/88234.png');">
          <div class="map-bg">
            <div class="map-box" style="background-image: url('<?=base_url()?>assets/image/<?=$slide_four->gambar_lokasi?>');background-size: cover;"></div>
          </div>

          <div class="lokasi-wrap">
            <h3>LOKASI RESEPSI</h3>
            <h5><?=$data_nikah->tempat_resepsi?></h5>
            <p><?=$data_nikah->alamat_resepsi?></p>
            <br/>
            <a href="<?=$data_nikah->url_map?>" target="_blank" class="button">Buka Google Maps</a>
          </div>
        </div>
      </section>

      <section class="page5" style="background-image: url('<?=$slide_five->is_bg==1?base_url()."assets/image/".$slide_five->background:""?>');">
        <?php
          if($slide_five->frame_1==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_five->frame?>" class="topleft">
        <?php
          }
        ?>

        <?php
          if($slide_five->frame_2==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_five->frame?>" class="topright">
        <?php
          }
        ?>
        
        <?php
          if($slide_five->frame_3==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_five->frame?>" class="bottomleft">
        <?php
          }
        ?>

        <?php
          if($slide_five->frame_4==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_five->frame?>" class="bottomright">
        <?php
          }
        ?>

        <div class="box-wrap center">
          <div class="box-wrap-justify">
            <h3>TURUT MENGUNDANG</h3>
            <ol class="list-tamu">
              <?php foreach ($tamu_undangan as $row) {?>
                <li><?=$row->nama?></li>
              <?php } ?>
            </ol>
          </div>
        </div>

      </section>

      <section class="page6">
        <div class="container-slider">
          <?php
            foreach ($slide_six as $row) {
          ?>
            <div class="mySlides">
              <div class="image-content">
                <img src="<?=base_url().'assets/image/gallery/'.$row->file?>" style='height: 100%; width: 100%; object-fit: contain'>
              </div>
            </div>
          <?php
            }
          ?>
          
          <!-- <a class="prev" onclick="plusSlides(-1)">❮</a>
          <a class="next" onclick="plusSlides(1)">❯</a> -->

          <div class="row">
            <?php
              $num = 1;
              foreach ($slide_six as $row) {
            ?>

            <div class="column">
              <img class="demo cursor" src="<?=base_url().'assets/image/gallery/'.$row->file?>" style="height: 100%; width: 100%; object-fit: cover;" onclick="currentSlide(<?=$num?>)" alt="The Woods">
            </div>
            
            <?php
                $num++;
              }
            ?>
          </div>
        </div>
      </section>

      <section class="page7" style="background-image: url('<?=$slide_seven->is_bg==1?base_url()."assets/image/".$slide_seven->background:""?>');">

        <?php
          if($slide_seven->frame_2==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_seven->frame?>" class="topright">
        <?php
          }
        ?>

        <?php
          if($slide_seven->frame_4==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_seven->frame?>" class="bottomright">
        <?php
          }
        ?>

        <div class="doa-mempelai" style="background-image: url('<?=base_url()?>assets/image/<?=$slide_seven->bg_main?>');">
          <div class="line"></div>
        </div>

        <div class="content-doa">
          <div class="content-doa-center">
            <p style="text-align: center;">
              Semoga Allah SWT memberi barokah dan menjadikan mempelai berdua dalam kebaikan keluarga sakinah, mawaddah dan warahmah.
              <br>
              <br>
              "Dan di antara tanda-tanda kekuasaan-Nya ialah Dia menciptakan untukmu isteri-isteri dari jenismu sendiri, supaya kamu cenderung dan merasa tenteram kepadanya, dan dijadikan-Nya diantaramu rasa kasih dan sayang. Sesungguhnya pada yang demikian itu benar-benar terdapat tanda-tanda bagi kaum yang berfikir"
              <br>
              (Ar-Rum Ayat 21)
              <br><br>Hormat Kami<br>
              <b><?=$data_nikah->mempelai_pria?> &amp; <?=$data_nikah->mempelai_wanita?></b>
              <br>
              <i>Wassalammu'alaikum Warahmatullahi Wabarakatuh</i>
            </p>
          </div>
        </div>

        <div class="sudut"></div>
      </section>

      <section class="page8" style="background-image: url('<?=$slide_eight->is_bg==1?base_url()."assets/image/".$slide_eight->background:""?>');">
        <?php
          if($slide_eight->frame_1==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_eight->frame?>" class="topleft">
        <?php
          }
        ?>

        <?php
          if($slide_eight->frame_2==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_eight->frame?>" class="topright">
        <?php
          }
        ?>
        
        <?php
          if($slide_eight->frame_3==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_eight->frame?>" class="bottomleft">
        <?php
          }
        ?>

        <?php
          if($slide_eight->frame_4==1){
        ?>
          <img src="<?=base_url()?>assets/image/<?=$slide_eight->frame?>" class="bottomright">
        <?php
          }
        ?>

        <div class="box-wrap center">
          <div class="box-wrap-justify">
            <h3>UCAPAN & DOA</h3>
            <form action="#" id="form" class="form-doa">
              <div class="form-group">
                <label>Nama Pengirim</label>
                <input type="text" class="form-control" value="<?=$invitation?$invitation['nama']:''?>" name="nama" required>
              </div>
              <div class="form-group">
                <label for="pwd">Isi Ucapan & Doa</label>
                <textarea class="form-control" id="pwd" rows="5" name="ucapan" required></textarea>
              </div>
              <div class="form-group">
                <label for="pwd">Konfirmasi Kehadiran</label>
                <select class="form-control" name="kehadiran" required>
                  <option value="Ya" selected>Ya</option>
                  <option value="Tidak">Tidak</option>
                </select>
              </div>
              <br/>
              <button type="button" id="kirim" class="button">Kirim</button>
            </form>
          </div>
        </div>

      </section>
    </div>
  </div>
</body>
<script>
  function triggerClick() {
    google.maps.event.trigger(markers, 'click');
  }

  function autoCenter() {
    //  Create a new viewpoint bound
    var bounds = new google.maps.LatLngBounds();
    //  Go through each...
      bounds.extend(markers.position);
    //  Fit these bounds to the map
    map.fitBounds(bounds);
  }
</script>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDAhJb12zZZ0Z0QEtQgQDRmCwzW2gV4oBI&callback=initMap">
    </script>
<script type="text/javascript" src="<?=base_url()?>assets/js/main.js"></script>

<script type="text/javascript">

  $('#kirim').on('click', function() {
  $('#kirim').text('Mengirim...'); //change button text
  $('#kirim').attr('disabled',true); //set button disable 

  var formData = new FormData($("#form")[0]);          
  $.ajax({
    url : "<?php echo site_url('welcome/ucapan')?>",
    type: "POST",
    data: formData,
    contentType: false,
    processData: false,
    enctype: 'multipart/form-data',
    cache:false,
    dataType:'json',
    success: function(data)
    {
      if(data.status) //if success close modal and reload ajax table
      {
        alert('Terimakasih Ucapan & Doa Anda');
      }
      else
      {
        alert('Gagal Mengirim, silahkan coba lagi');
      }
      $('#kirim').text('Kirim'); //change button text
      $('#kirim').attr('disabled',false); //set button enable 


    },
    error: function (jqXHR, textStatus, errorThrown)
    {
      alert('Error adding / update data');
      $('#kirim').text('Kirim'); //change button text
      $('#kirim').attr('disabled',false); //set button enable 

    }
  });
});
</script>
</html>