<script src="<?=base_url()?>/assets/admin/js/plugins/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<style type="text/css">
	.error{
		font-size: 12px;
		color: red;
	}
</style>
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8"></div>
<div class="container-fluid mt--7">
  <div class="row">
    <div class="col-md-12 mb-5 mb-xl-0">
      <div class="card bg-secondary shadow">
        <div class="card-body">
        	<form action="#" id="form" class="form-horizontal">
	          <h6 class="heading-small text-muted mb-4">Data Mempelai</h6>
	          <div class="form-group">
	            <label class="form-control-label" for="input-username">Nama Mempelai Pria</label>
	            <input type="text" class="form-control" placeholder="Mempelai Pria" name="mempelai_pria" value="<?=$data_nikah->mempelai_pria?>">
	          </div>
	          <div class="form-group">
	            <label class="form-control-label" for="input-username">Nama Mempelai Wanita</label>
	            <input type="text" class="form-control" placeholder="Mempelai Wanita" name="mempelai_wanita" value="<?=$data_nikah->mempelai_wanita?>">
	          </div>

	          <hr class="my-4">

	          <h6 class="heading-small text-muted mb-4">Data Orang Tua Mempelai</h6>
	          <div class="row">
	          	<div class="col-md-6">
	          		<div class="form-group">
			            <label class="form-control-label" for="input-username">Putra Ke</label>
			            <select class="form-control" name="putra_ke">
			            	<option selected hidden value="">- Putra Ke Berapa -</option>
			            	<option <?=$data_nikah->putra_ke==='Pertama'?'selected':''?> value="Pertama">Pertama</option>
			            	<option <?=$data_nikah->putra_ke==='Kedua'?'selected':''?> value="Kedua">Kedua</option>
			            	<option <?=$data_nikah->putra_ke==='Ketiga'?'selected':''?> value="Ketiga">Ketiga</option>
			            	<option <?=$data_nikah->putra_ke==='Keempat'?'selected':''?> value="Keempat">Keempat</option>
			            	<option <?=$data_nikah->putra_ke==='Kelima'?'selected':''?> value="Kelima">Kelima</option>
			            	<option <?=$data_nikah->putra_ke==='Keenam'?'selected':''?> value="Keenam">Keenam</option>
			            	<option <?=$data_nikah->putra_ke==='Ketujuh'?'selected':''?> value="Ketujuh">Ketujuh</option>
			            	<option <?=$data_nikah->putra_ke==='Kedelapan'?'selected':''?> value="Kedelapan">Kedelapan</option>
			            	<option <?=$data_nikah->putra_ke==='Kesembilan'?'selected':''?> value="Kesembilan">Kesembilan</option>
			            	<option <?=$data_nikah->putra_ke==='Kesepuluh'?'selected':''?> value="Kesepuluh">Kesepuluh</option>
			            </select>
			          </div>
			          <div class="form-group">
			            <label class="form-control-label" for="input-username">Nama Ayah Mempelai Pria</label>
			            <input type="text" class="form-control" placeholder="Ayah Mempelai Pria" name="ayah_pria" value="<?=$data_nikah->ayah_pria?>">
			          </div>
			          <div class="form-group">
			            <label class="form-control-label" for="input-username">Nama Ibu Mempelai Pria</label>
			            <input type="text" class="form-control" placeholder="Ibu Mempelai Pria" name="ibu_pria" value="<?=$data_nikah->ibu_pria?>">
			          </div>
			        </div>
			        <div class="col-md-6">
			        	<div class="form-group">
			            <label class="form-control-label" for="input-username">Putri Ke</label>
			            <select class="form-control" name="putri_ke">
			            	<option selected hidden value="">- Putri Ke Berapa -</option>
			            	<option <?=$data_nikah->putri_ke==='Pertama'?'selected':''?> value="Pertama">Pertama</option>
			            	<option <?=$data_nikah->putri_ke==='Kedua'?'selected':''?> value="Kedua">Kedua</option>
			            	<option <?=$data_nikah->putri_ke==='Ketiga'?'selected':''?> value="Ketiga">Ketiga</option>
			            	<option <?=$data_nikah->putri_ke==='Keempat'?'selected':''?> value="Keempat">Keempat</option>
			            	<option <?=$data_nikah->putri_ke==='Kelima'?'selected':''?> value="Kelima">Kelima</option>
			            	<option <?=$data_nikah->putri_ke==='Keenam'?'selected':''?> value="Keenam">Keenam</option>
			            	<option <?=$data_nikah->putri_ke==='Ketujuh'?'selected':''?> value="Ketujuh">Ketujuh</option>
			            	<option <?=$data_nikah->putri_ke==='Kedelapan'?'selected':''?> value="Kedelapan">Kedelapan</option>
			            	<option <?=$data_nikah->putri_ke==='Kesembilan'?'selected':''?> value="Kesembilan">Kesembilan</option>
			            	<option <?=$data_nikah->putri_ke==='Kesepuluh'?'selected':''?> value="Kesepuluh">Kesepuluh</option>
			            </select>
			          </div>
			          <div class="form-group">
			            <label class="form-control-label" for="input-username">Nama Ayah Mempelai Wanita</label>
			            <input type="text" class="form-control" placeholder="Ayah Mempelai Wanita" name="ayah_wanita" value="<?=$data_nikah->ayah_wanita?>">
			          </div>
			          <div class="form-group">
			            <label class="form-control-label" for="input-username">Nama Ibu Mempelai Wanita</label>
			            <input type="text" class="form-control" placeholder="Ibu Mempelai Wanita" name="ibu_wanita" value="<?=$data_nikah->ibu_wanita?>">
			          </div>
			        </div>
			      </div>

	        <hr class="my-4">

	        <h6 class="heading-small text-muted mb-4">Data Acara</h6>
	          <div class="row">
	          	<div class="col-md-6">
	          		<div class="row">
	          			<div class="col-md-7">
					          <div class="form-group">
					            <label class="form-control-label" for="input-username">Tanggal Akad</label>
					            <div class="form-group">
										    <div class="input-group">
									        <div class="input-group-prepend">
									        	<span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
									        </div>
									        <input class="form-control datepicker" placeholder="Tanggal" type="text" style="padding-left: 10px" name="tanggal_akad" value="<?=date_format(date_create($data_nikah->tanggal_akad), 'm/d/Y')?>">
										    </div>
											</div>
					          </div>
					      	</div>

					      	<div class="col-md-5">
					          <div class="form-group">
					            <label class="form-control-label" for="input-username">Jam</label>
					            <div class="form-group">
										    <div class="input-group">
									        <div class="input-group-prepend">
									            <span class="input-group-text"><i class="fa fa-clock"></i></span>
									        </div>
									        <input class="form-control timepicker" placeholder="Jam" type="index" style="padding-left: 10px" name="jam_akad" value="<?=date_format(date_create($data_nikah->tanggal_akad), 'H:i')?>">
										    </div>
											</div>
					          </div>
					      	</div>
					    	</div>

					    	<div class="form-group">
			            <label class="form-control-label" for="input-username">Tempat Akad</label>
			            <input type="text" class="form-control" placeholder="Tempat Akad" name="tempat_akad" value="<?=$data_nikah->tempat_akad?>">
			          </div>
			          <div class="form-group">
			            <label class="form-control-label" for="input-username">Alamat Akad</label>
			            <textarea class="form-control" placeholder="Alamat Akad" rows="5" name="alamat_akad" value="<?=$data_nikah->alamat_akad?>"><?=$data_nikah->alamat_akad?></textarea>
			          </div>

			          <div class="row">
			          	<div class="col-md-12">
			          		<div class="form-group">
					            <label class="form-control-label" for="input-username">Url Google Maps</label>
					            <input type="text" class="form-control" placeholder="Url Google Maps" name="url_map" value="<?=$data_nikah->url_map?>">
					         </div>
			          	</div>
			          </div>
			        </div>

			        <div class="col-md-6">
			          <div class="row">
	          			<div class="col-md-7">
				          	<div class="form-group">
				            	<label class="form-control-label" for="input-username">Tanggal Akad</label>
				            	<div class="form-group">
										    <div class="input-group">
									        <div class="input-group-prepend">
									         	<span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
									        </div>
									        <input class="form-control datepicker" placeholder="Tanggal" type="text" style="padding-left: 10px" name="tanggal_resepsi" value="<?=date_format(date_create($data_nikah->tanggal_resepsi), 'm/d/Y')?>">
										    </div>
											</div>
					          </div>
					      	</div>

					      	<div class="col-md-5">
					          <div class="form-group">
					            <label class="form-control-label" for="input-username">Jam</label>
					            <div class="form-group">
										    <div class="input-group">
									        <div class="input-group-prepend">
									          <span class="input-group-text"><i class="fa fa-clock"></i></span>
									        </div>
									        <input class="form-control timepicker" placeholder="Jam" type="index" style="padding-left: 10px" name="jam_resepsi" value="<?=date_format(date_create($data_nikah->tanggal_akad), 'H:i')?>">
										    </div>
											</div>
					          </div>
					      	</div>	
					    	</div>

					    	<div class="form-group">
			            <label class="form-control-label" for="input-username">Tempat Resepsi</label>
			            <input type="text" class="form-control" placeholder="Tempat Resepsi" name="tempat_resepsi" value="<?=$data_nikah->tempat_resepsi?>">
			          </div>
			          <div class="form-group">
			            <label class="form-control-label" for="input-username">Alamat Resepsi</label>
			            <textarea class="form-control" placeholder="Alamat Resepsi" rows="5" name="alamat_resepsi" value="<?=$data_nikah->alamat_resepsi?>"><?=$data_nikah->alamat_resepsi?></textarea>
			          </div>

			        </div>
			      </div>

		      	<hr class="my-4">

	          <h6 class="heading-small text-muted mb-4">Turut Mengundang</h6>
	          <div class="container-undangan">
	          	<label class="form-control-label" for="input-username">Tambahkan Tamu Istimewa  &nbsp; <button class="btn btn-sm btn-success add_form_field">+ Tambah</button></label>

	          	<?php
		          	if(count($tamu_undangan)>0){
		          		foreach ($tamu_undangan as $row) {
		          ?>
				          	<div class="input-group mb-3">
											 <input type="text" class="form-control" placeholder="Masukan Nama" name="undangan[]" value="<?=$row->nama?>">
											 <div class="input-group-append">
											   <button class="btn btn-danger delete" type="button"><i class="fa fa-trash"></i></button>
											 </div>
										</div>
		          <?php
		          		}
		          	}
		          ?>
	          </div>

	          <hr class="my-4">
	        </form>
	        <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
	      </div>
      </div>
    </div>
  </div>

<script type="text/javascript">
	const setCaret = function(elm, pos) {
	  if(elm.createTextRange) {
	    var range = elm.createTextRange();
	    range.move('character', pos);
	    range.select();
	  } else {
	    if(typeof(elm.selectionStart) !== "undefined") {
	      elm.focus();
	      elm.setSelectionRange(pos, pos);
	    } else {
	      elm.focus();
	    }
	  }
	}
	const update = function(elm, val, pos) {
	  requestAnimationFrame(function() {
	    elm.value = val;
	  });
	  requestAnimationFrame(function() {
	    setCaret(elm, pos);
	  });
	}
	const getFormattedValue = function(state) {
	  const newStateInput = state.input.reduce(function(acc, curr) {
	    if (acc.length === 2) {
	      acc += ":";
	    }
	    if (curr !== null) {
	      return acc + String(curr);
	    }
	      return acc;
	  }, "")
	  return newStateInput;
	}
	const getFormattedPosition = function(position) {
	  switch(position) {
	    case 0:
	    case 1:
	      return position;
	    case 2:
	      return 3;
	    case 3:
	      return 5;
	  }
	}
	const updateInput = function(state, newInt, forceTimeOfDay) {
	  let newInput = state.input.slice();
	  newInput[state.position] = parseInt(newInt, 10);
	  if (forceTimeOfDay) {
	    const hours = 
	      newInput[0] !== null && newInput[1] !== null
	        ? newInput[0] * 10 + newInput[1]
	        : null;
	    const minutes =
	      newInput[2] !== null && newInput[3] !== null
	        ? newInput[2] * 10 + newInput[3]
	        : null;
	    if (hours && hours > 24) {
	      newInput[0] = 2;
	      newInput[1] = 4;
	    }
	    if (minutes && minutes > 59) {
	      newInput[2] = 5;
	      newInput[3] = 9;
	    }
	  }
	  return newInput;
	}
	const stop = function(event) {
	  event.preventDefault();
	  event.stopImmediatePropagation();
	  event.stopPropagation();
	}
	var makeItTime = function(options) {
	  let input;
	  if (typeof(options.input) === "string") {
	    input = document.getElementById(options.input);
	  } else {
	    input = options.input;
	  }
	  input.size = 5;
	  input.placeholder = "00:00";
	  let managedKeyCodes = { BACKSPACE: 8
	                        , SHIFT: 16
	                        , SPACE: 32
	                        , END: 35
	                        , HOME: 36
	                        , LEFT: 37
	                        , RIGHT: 39
	                        , DELETE: 46
	                        }
	  for (let i = 48; i < 58; i++) {
	    managedKeyCodes[`NUM_${i - 48}`] = i;
	  }
	  for (let i = 96; i < 106; i++) {
	    managedKeyCodes[`NUMPAD_${i - 96}`] = i;
	  }
	  const keyCodeList = Object.values(managedKeyCodes);
	  let state = {
	    input: [null, null, null, null],
	    position: 0
	  }
	  input.addEventListener("keydown", function(event) {
	    if (event.keyCode > 47) {
	      stop(event);
	    }
	    if (keyCodeList.includes(event.keyCode)) {
	      if ((event.keyCode > 47 && event.keyCode < 59)
	          || (event.keyCode > 95 && event.keyCode < 107)) {
	        state.input = updateInput(state, String.fromCharCode((event.keyCode % 48) + 48), options.forceTimeOfDay);
	        state.position = state.position < 3 ? state.position + 1 : 3;
	      }
	      switch(event.keyCode) {
	        case managedKeyCodes.LEFT:
	          state.position = state.position > 0 ? state.position - 1 : 0;
	          break;
	        case managedKeyCodes.RIGHT:
	          state.position = state.position < 3 ? state.position + 1 : 3;
	          break;
	        case managedKeyCodes.HOME:
	          state.position = 0;
	          break;
	        case managedKeyCodes.END:
	          state.position = 3;
	          break;
	        case managedKeyCodes.DELETE:
	          state.input[state.position] = null;
	          state.position = state.position < 3 ? state.position + 1 : 3;
	          break;
	        case managedKeyCodes.BACKSPACE:
	          if (state.input[state.position] === null) {
	            state.position = state.position > 0 ? state.position - 1 : 0;
	          }
	          state.input[state.position] = null;
	          break;
	      }
	      update(input, getFormattedValue(state), getFormattedPosition(state.position));
	    }
	  })
	  input.addEventListener("keyup", stop);
	  input.addEventListener("change", stop);
	  input.addEventListener("click", function(event) {
	    const pos = this.selectionStart;
	    switch(pos) {
	      case 0:
	      case 1:
	        state.position = pos;
	        break;
	      case 2:
	      case 3:
	        state.position = 2;
	        break;
	      default:
	        state.position = 3;
	        break;
	    }
	    update(input, getFormattedValue(state), getFormattedPosition(state.position));
	  })
	}
	makeItTime({
	  input: document.getElementsByClassName("timepicker")[0],
	  forceTimeOfDay: true
	});
	makeItTime({
	  input: document.getElementsByClassName("timepicker")[1],
	  forceTimeOfDay: false
	});
</script>

<script type="text/javascript">
	$(document).ready(function() {
	    var wrapper = $(".container-undangan");
	    var add_button = $(".add_form_field");

	    var x = 1;
	    $(add_button).click(function(e) {
	        e.preventDefault();
	        $(wrapper).append('<div class="input-group mb-3">'
								  +'<input type="text" class="form-control" placeholder="Masukan Nama" name="undangan[]">'
								  +'<div class="input-group-append">'
								    +'<button class="btn btn-danger delete" type="button"><i class="fa fa-trash"></i></button>'
								  +'</div>'
								+'</div>');
	    });

	    $(wrapper).on("click", ".delete", function(e) {
	        e.preventDefault();
	        $(this).closest('.input-group').remove();
	        x--;
	    });
	});
</script>

<script type="text/javascript">
	function save()
	{
	    $('#btnSave').text('saving...'); //change button text
	    $('#btnSave').attr('disabled',true); //set button disable 
	 
	    // ajax adding data to database
	    var formData = new FormData($('#form')[0]);
	    $.ajax({
	        url : "<?php echo site_url('admin/data_nikah/save')?>",
	        type: "POST",
	        data: formData,
	        contentType: false,
	        processData: false,
	        dataType: "JSON",
	        success: function(data)
	        {
	 						//remove error
	           	document.querySelectorAll('.error').forEach(e => e.remove());

	            if(data.status) //if success close modal and reload ajax table
	            {
	                alert('Data Berhasil Disimpan');
	            }
	            else
	            {

	                for (var i = 0; i < data.inputerror.length; i++) 
	                {
	                	if(data.inputerror[i]==='tanggal_akad'||data.inputerror[i]==='jam_akad'||data.inputerror[i]==='tanggal_resepsi'||data.inputerror[i]==='jam_resepsi'){
	                		$('[name="'+data.inputerror[i]+'"]').parent().parent().append("<span class='error'>"+data.error_string[i]+"</span>");
	                	}else{
	                		$("<span class='error'>"+data.error_string[i]+"</span>").insertBefore('[name="'+data.inputerror[i]+'"]');
	                	}
	                }

	                alert('Mohon Perikasi Kembali Data Anda');
	            }
	            $('#btnSave').text('Save'); //change button text
	            $('#btnSave').attr('disabled',false); //set button enable 
	 
	 
	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	            alert('Error adding / update data');
	            $('#btnSave').text('Save'); //change button text
	            $('#btnSave').attr('disabled',false); //set button enable 
	 
	        }
	    });
	}
</script>