<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.js"></script>
<style type="text/css">
	.error{
		color: red;
		font-size: .9em;
	}
</style>
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8"></div>
<div class="container-fluid mt--7">
  <div class="row">
    <div class="col-md-12 mb-5 mb-xl-0">
      <div class="card shadow">
        <div class="card-body">
          <div class="row">
          	<div class="col-md-6">
          		<div class="form-group">
		            <label class="form-control-label" for="input-username">Nama <span class="error" id="error_nama"></span></label>
		            <input type="text" class="form-control" placeholder="Masukan Nama" id="nama" oninput="this.value = this.value.replace(/[^A-Za-z, ]/, '')">
		          </div>
          	</div>
          </div>
          <div class="row">
          	<div class="col-md-6">
          		<div class="form-group">
		            <label class="form-control-label" for="input-username">Gelar (Optional)</label>
		            <input type="text" class="form-control" placeholder="Masukan Gelar" id="gelar" oninput="this.value = this.value.replace(/[^A-Za-z, ]/, '')">
		          </div>
          	</div>
          </div>
          <div class="row">
          	<div class="col-md-6">
          		<div class="form-group">
		            <label class="form-control-label" for="input-username">Alamat <span class="error" id="error_alamat"></span></label>
		            <input type="text" class="form-control" placeholder="Alamat" id="alamat" oninput="this.value = this.value.replace(/[^A-Za-z, ]/, '')">
		          </div>
          	</div>
          </div>
          <button type="button" id="btnSave" onclick="generate()" class="btn btn-primary">Generate Link</button>
          <br/><br/>
          <div class="row">
          	<div class="col-md-12">
          		<div class="form-group">
		            <label class="form-control-label" for="input-username">Link</label>
		            <div class="input-group mb-3">
									 <input type="text" class="form-control" id="link">
									 <div class="input-group-append">
									   <button class="btn btn-danger delete" type="button" onclick="copy()">Copy</button>
									 </div>
								</div>
		          </div>
          	</div>
          </div>
        </div>
      </div>
    </div>
  </div>


<script type="text/javascript">
	function generate() {
		$('#error_nama').html('');
		$('#error_alamat').html('');
		$("#link").val('');

		var nama = $("#nama").val();
		var gelar = $("#gelar").val();
		var alamat = $("#alamat").val();

		if(nama=='' || alamat==''){
			if(nama==''){
				$('#error_nama').html('Harus Diisi!');
			}else{
				$('#error_nama').html('');
			}

			if(alamat==''){
				$('#error_alamat').html('Harus Diisi!');
			}else{
				$('#error_alamat').html('');
			}
		}else{
			$('#error_nama').html('');
			$('#error_alamat').html('');

			var new_nama = nama.replace(/\ /g,'+');
			if(gelar!=''){
				var new_gelar = '(+'+gelar.replace(/\ /g,'+')+'+)';
			}else{
				var new_gelar = '';
			}
			var new_alamat = alamat.replace(/\ /g,'+');
			var url = '<?=base_url()?>invitation/'+new_nama+'-'+new_gelar+'-'+new_alamat;

			$("#link").val(url);
		}
	}

	function copy() {
	  /* Get the text field */
	  var copyText = document.getElementById("link");

	  /* Select the text field */
	  copyText.select();
	  copyText.setSelectionRange(0, 99999); /*For mobile devices*/

	  /* Copy the text inside the text field */
	  document.execCommand("copy");

	  /* Alert the copied text */
	  alert("Link Berhasil Di Copy");
	}
</script>