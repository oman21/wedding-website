<style type="text/css">
  .note-form{
    font-size: .8em;
  }
</style>
<div class="header bg-gradient-primary pb-8 pt-5 pt-md-8"></div>
<div class="container-fluid mt--7">
  <div class="row">
    <div class="col-md-12 mb-5 mb-xl-0">
      <div class="card bg-secondary shadow">
        <div class="card-header bg-transparent">
          <div class="row align-items-center">
            <div class="col">
              <h2 class="mb-0">Cover</h2>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form action="#" id="formCover" class="form-horizontal">
            <div class="row">
              <div class="col-md-4 offset-md-4" style="text-align: center;">
                <h6 class="heading-small text-muted mb-4">Logo Undangan</h6>
                <img src="<?=$cover->logo==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$cover->logo?>" class="rounded-circle" style="width: 120px; height: 120px; margin: auto; border:1px solid #ccc" id="foto_cover_logo">
                <br/><br/>
                <input type="file" class="form-control" name="foto_cover_logo" onChange="readURL(this, '#foto_cover_logo')">
              </div>
            </div>
            <hr class="my-4">
            <div class="row">
              <div class="col-md-12">
                <h6 class="heading-small text-muted mb-4">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="cover_is_bg" <?=$cover->is_bg?"checked":""?> name="cover_is_bg">
                    <label class="custom-control-label" for="cover_is_bg">Gunakan Background</label>
                  </div>
                </h6>

                <img src="<?=$cover->background==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$cover->background?>" style="width: 100%; height: 335px; margin: auto; border:1px solid #ccc; object-fit: cover" id="foto_cover_bg">
                <input type="file" class="form-control" name="foto_cover_bg" onChange="readURL(this, '#foto_cover_bg')">
              </div>
            </div>
            <hr class="my-4">
            <div class="row">
              <div class="col-md-4" style="text-align: center;">
                <img src="<?=$cover->frame==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$cover->frame?>" style="width: 200px; height: 200px; margin: auto; border:1px solid #ccc" id="foto_cover_frame">
                <input type="file" class="form-control" name="foto_cover_frame" onChange="readURL(this, '#foto_cover_frame')" style="width:200px; margin: auto;">
                <i class="note-form">Rekomendasi ukuran 283 x 283 pixel</i>
              </div>
              <div class="col-md-8">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="cover_frame_1" <?=$cover->frame_1?"checked":""?> name="cover_frame_1">
                  <label class="custom-control-label" for="cover_frame_1">Gunakan Frame Atas Kiri</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="cover_frame_2" <?=$cover->frame_2?"checked":""?> name="cover_frame_2">
                  <label class="custom-control-label" for="cover_frame_2">Gunakan Frame Atas Kanan</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="cover_frame_3" <?=$cover->frame_3?"checked":""?> name="cover_frame_3">
                  <label class="custom-control-label" for="cover_frame_3">Gunakan Frame Bawah Kiri</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="cover_frame_4" <?=$cover->frame_4?"checked":""?> name="cover_frame_4">
                  <label class="custom-control-label" for="cover_frame_4">Gunakan Frame Bawah Kanan</label>
                </div>
              </div>
            </div>
            <br/>
            <button type="button" id="btnSaveCover" class="btn btn-primary">Save Cover</button>
            <a class="btn btn-success text-white" href="<?=base_url()?>" target="_blank">Preview</a>
          </form>
        </div>
      </div>
    </div>
  </div>
  <br/>
  <div class="row">
    <div class="col-md-12 mb-5 mb-xl-0">
      <div class="card bg-secondary shadow">
        <div class="card-header bg-transparent">
          <div class="row align-items-center">
            <div class="col">
              <h2 class="mb-0">Slide 1</h2>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form action="#" id="formSlide1" class="form-horizontal">
            <div class="row">
              <div class="col-md-4 offset-md-4" style="text-align: center;">
                <h6 class="heading-small text-muted mb-4">Logo Undangan</h6>
                <img src="<?=$slide_one->logo==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_one->logo?>" class="rounded-circle" style="width: 120px; height: 120px; margin: auto; border:1px solid #ccc" id="foto_slide_one_logo">
                <br/><br/>
                <input type="file" class="form-control" name="foto_slide_one_logo" id="input_slide_one_logo">
              </div>
            </div>
            <hr class="my-4">
            <div class="row">
              <div class="col-md-4" style="text-align: center;">
                <h6 class="heading-small text-muted mb-4">Foto Mempelai</h6>
                <img src="<?=$slide_one->foto_mempelai==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_one->foto_mempelai?>" style="width: 200px; height: 235px; margin: auto; border:1px solid #ccc" id="foto_slide_one_mempelai">
                <br/><br/>
                <input type="file" class="form-control" name="foto_slide_one_mempelai" id="input_slide_one_mempelai">
                <i class="note-form">Rekomendasi ukuran 900 x 1057 pixel</i>
              </div>
              <div class="col-md-8" style="text-align: center;">
                <h6 class="heading-small text-muted mb-4">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="slide_1_frame_is_bg" <?=$slide_one->is_bg?"checked":""?> name="slide_1_frame_is_bg">
                    <label class="custom-control-label" for="slide_1_frame_is_bg">Gunakan Background</label>
                  </div>
                </h6>

                <img src="<?=$slide_one->background==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_one->background?>" style="width: 100%; height: 235px; margin: auto; border:1px solid #ccc; object-fit: cover" id="foto_slide_one_bg">
                <br/><br/>
                <input type="file" class="form-control" name="foto_slide_one_bg" id="input_slide_one_bg">
              </div>
            </div>
            <hr class="my-4">
            <div class="row">
              <div class="col-md-4" style="text-align: center;">
                <img src="<?=$slide_one->frame==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_one->frame?>" style="width: 200px; height: 200px; margin: auto; border:1px solid #ccc" id="foto_slide_one_frame">
                <input type="file" class="form-control" name="foto_slide_one_frame" id="input_slide_one_frame" style="width:200px; margin: auto;">
                <i class="note-form">Rekomendasi ukuran 283 x 283 pixel</i>
              </div>
              <div class="col-md-8">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_1_frame_1" <?=$slide_one->frame_1?"checked":""?> name="slide_1_frame_1">
                  <label class="custom-control-label" for="slide_1_frame_1">Gunakan Frame Atas Kiri</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_1_frame_2" <?=$slide_one->frame_2?"checked":""?> name="slide_1_frame_2">
                  <label class="custom-control-label" for="slide_1_frame_2">Gunakan Frame Atas Kanan</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_1_frame_3" <?=$slide_one->frame_3?"checked":""?> name="slide_1_frame_3">
                  <label class="custom-control-label" for="slide_1_frame_3">Gunakan Frame Bawah Kiri</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_1_frame_4" <?=$slide_one->frame_4?"checked":""?> name="slide_1_frame_4">
                  <label class="custom-control-label" for="slide_1_frame_4">Gunakan Frame Bawah Kanan</label>
                </div>
              </div>
            </div>
            <hr class="my-4">
            <br/>
            <button type="button" id="btnSaveSlide1" class="btn btn-primary">Save Slide 1</button>
            <a class="btn btn-success text-white" href="<?=base_url()?>#1" target="_blank">Preview</a>
          </form>
        </div>
      </div>
    </div>
  </div>
  <br/>
  <div class="row">
    <div class="col-md-12 mb-5 mb-xl-0">
      <div class="card bg-secondary shadow">
        <div class="card-header bg-transparent">
          <div class="row align-items-center">
            <div class="col">
              <h2 class="mb-0">Slide 2</h2>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form action="#" id="formSlide2" class="form-horizontal" enctype="multipart">
            <div class="row">
              <div class="col-md-6" style="text-align: center;">
                <h6 class="heading-small text-muted mb-4">Foto Mempelai Pria</h6>
                <img src="<?=$slide_two->foto_mempelai_pria==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_two->foto_mempelai_pria?>" class="rounded-circle" style="width: 120px; height: 120px; margin: auto; border:1px solid #ccc" id="foto_mempelai_pria">
                <br/><br/>
                <b><?=$data_nikah->mempelai_pria?></b>
                <input type="file" class="form-control" name="foto_mempelai_pria" id="input_mempelai_pria">
              </div>
              <div class="col-md-6" style="text-align: center;">
                <h6 class="heading-small text-muted mb-4">Foto Mempelai Wanita</h6>
                <img src="<?=$slide_two->foto_mempelai_wanita==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_two->foto_mempelai_wanita?>" class="rounded-circle" style="width: 120px; height: 120px; margin: auto; border: 1px solid #ccc" id="foto_mempelai_wanita">
                <br/><br/>
                <b><?=$data_nikah->mempelai_wanita?></b>
                <input type="file" class="form-control" name="foto_mempelai_wanita" id="input_mempelai_wanita">
              </div>
            </div>
            <hr class="my-4">
            <div class="row">
              <div class="col-md-12">
                <h6 class="heading-small text-muted mb-4">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="slide_2_is_bg" <?=$slide_two->is_bg?"checked":""?> name="slide_2_is_bg">
                    <label class="custom-control-label" for="slide_2_is_bg">Gunakan Background</label>
                  </div>
                </h6>

                <img src="<?=$slide_two->background==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_two->background?>" style="width: 100%; height: 335px; margin: auto; border:1px solid #ccc; object-fit: cover" id="foto_slide_two_bg">
                <input type="file" class="form-control" name="foto_slide_two_bg" onChange="readURL(this, '#foto_slide_two_bg')">
              </div>
            </div>
            <hr class="my-4">
            <div class="row">
              <div class="col-md-4" style="text-align: center;">
                <img src="<?=$slide_two->frame==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_two->frame?>" style="width: 200px; height: 200px; margin: auto; border:1px solid #ccc" id="foto_slide_two_frame">
                <input type="file" class="form-control" name="foto_slide_two_frame" onChange="readURL(this, '#foto_slide_two_frame')" style="width:200px; margin: auto;">
                <i class="note-form">Rekomendasi ukuran 283 x 283 pixel</i>
              </div>
              <div class="col-md-8">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_2_frame_1" <?=$slide_two->frame_1?"checked":""?> name="slide_2_frame_1">
                  <label class="custom-control-label" for="slide_2_frame_1">Gunakan Frame Atas Kiri</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_2_frame_2" <?=$slide_two->frame_2?"checked":""?> name="slide_2_frame_2">
                  <label class="custom-control-label" for="slide_2_frame_2">Gunakan Frame Atas Kanan</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_2_frame_3" <?=$slide_two->frame_3?"checked":""?> name="slide_2_frame_3">
                  <label class="custom-control-label" for="slide_2_frame_3">Gunakan Frame Bawah Kiri</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_2_frame_4" <?=$slide_two->frame_4?"checked":""?> name="slide_2_frame_4">
                  <label class="custom-control-label" for="slide_2_frame_4">Gunakan Frame Bawah Kanan</label>
                </div>
              </div>
            </div>
            <br/>
            <button type="button" id="btnSaveSlide2" class="btn btn-primary">Save Slide 2</button>
            <a class="btn btn-success text-white" href="<?=base_url()?>#2" target="_blank">Preview</a>
          </form>
        </div>
      </div>
    </div>
  </div>
  <br/>
  <div class="row">
    <div class="col-md-12 mb-5 mb-xl-0">
      <div class="card bg-secondary shadow">
        <div class="card-header bg-transparent">
          <div class="row align-items-center">
            <div class="col">
              <h2 class="mb-0">Slide 3</h2>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form action="#" id="formSlide3" class="form-horizontal">
            <div class="row">
              <div class="col-md-4 offset-md-4" style="text-align: center;">
                <h6 class="heading-small text-muted mb-4">Foto Slide 3 Content</h6>
                <img src="<?=$slide_three->content==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_three->content?>" style="width: 200px; height: 280px; margin: auto; border:1px solid #ccc;object-fit: cover;" id="foto_slide_three_content">
                <br/><br/>
                <input type="file" class="form-control" name="foto_slide_three_content" id="input_slide_three_content">
                <i class="note-form">Rekomendasi ukuran 1200 x 796 pixel</i>
              </div>
            </div>
            <hr class="my-4">
            <div class="row">
              <div class="col-md-12">
                <h6 class="heading-small text-muted mb-4">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="slide_3_is_bg" <?=$slide_three->is_bg?"checked":""?> name="slide_3_is_bg">
                    <label class="custom-control-label" for="slide_3_is_bg">Gunakan Background</label>
                  </div>
                </h6>

                <img src="<?=$slide_three->background==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_three->background?>" style="width: 100%; height: 335px; margin: auto; border:1px solid #ccc; object-fit: cover" id="foto_slide_three_bg">
                <input type="file" class="form-control" name="foto_slide_three_bg" onChange="readURL(this, '#foto_slide_three_bg')">
              </div>
            </div>
            <hr class="my-4">
            <div class="row">
              <div class="col-md-4" style="text-align: center;">
                <img src="<?=$slide_three->frame==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_three->frame?>" style="width: 200px; height: 200px; margin: auto; border:1px solid #ccc" id="foto_slide_three_frame">
                <input type="file" class="form-control" name="foto_slide_three_frame" onChange="readURL(this, '#foto_slide_three_frame')" style="width:200px; margin: auto;">
                <i class="note-form">Rekomendasi ukuran 283 x 283 pixel</i>
              </div>
              <div class="col-md-8">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_3_frame_1" <?=$slide_three->frame_1?"checked":""?> name="slide_3_frame_1">
                  <label class="custom-control-label" for="slide_3_frame_1">Gunakan Frame Atas Kiri</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_3_frame_2" <?=$slide_three->frame_2?"checked":""?> name="slide_3_frame_2">
                  <label class="custom-control-label" for="slide_3_frame_2">Gunakan Frame Atas Kanan</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_3_frame_3" <?=$slide_three->frame_3?"checked":""?> name="slide_3_frame_3">
                  <label class="custom-control-label" for="slide_3_frame_3">Gunakan Frame Bawah Kiri</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_3_frame_4" <?=$slide_three->frame_4?"checked":""?> name="slide_3_frame_4">
                  <label class="custom-control-label" for="slide_3_frame_4">Gunakan Frame Bawah Kanan</label>
                </div>
              </div>
            </div>
            <br/>
            <button type="button" id="btnSaveSlide3" class="btn btn-primary">Save Slide 3</button>
            <a class="btn btn-success text-white" href="<?=base_url()?>#3" target="_blank">Preview</a>
          </form>
        </div>
      </div>
    </div>
  </div>
  <br/>
  <div class="row">
    <div class="col-md-12 mb-5 mb-xl-0">
      <div class="card bg-secondary shadow">
        <div class="card-header bg-transparent">
          <div class="row align-items-center">
            <div class="col">
              <h2 class="mb-0">Slide 4</h2>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form action="#" id="formSlide4" class="form-horizontal">
            <div class="row">
              <div class="col-md-12">
                <h6 class="heading-small text-muted mb-4">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="slide_4_is_bg" <?=$slide_four->is_bg?"checked":""?> name="slide_4_is_bg">
                    <label class="custom-control-label" for="slide_4_is_bg">Gunakan Background</label>
                  </div>
                </h6>

                <img src="<?=$slide_four->background==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_four->background?>" style="width: 100%; height: 335px; margin: auto; border:1px solid #ccc; object-fit: cover" id="foto_slide_four_bg">
                <input type="file" class="form-control" name="foto_slide_four_bg" onChange="readURL(this, '#foto_slide_four_bg')">
              </div>
            </div>
            <hr class="my-4">
            <div class="row">
              <div class="col-md-4" style="text-align: center;">
                <img src="<?=$slide_four->frame==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_four->frame?>" style="width: 200px; height: 200px; margin: auto; border:1px solid #ccc" id="foto_slide_four_frame">
                <input type="file" class="form-control" name="foto_slide_four_frame" onChange="readURL(this, '#foto_slide_four_frame')" style="width:200px; margin: auto;">
                <i class="note-form">Rekomendasi ukuran 283 x 283 pixel</i>
              </div>
              <div class="col-md-8">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_4_frame_1" <?=$slide_four->frame_1?"checked":""?> name="slide_4_frame_1">
                  <label class="custom-control-label" for="slide_4_frame_1">Gunakan Frame Atas Kiri</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_4_frame_2" <?=$slide_four->frame_2?"checked":""?> name="slide_4_frame_2">
                  <label class="custom-control-label" for="slide_4_frame_2">Gunakan Frame Atas Kanan</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_4_frame_3" <?=$slide_four->frame_3?"checked":""?> name="slide_4_frame_3">
                  <label class="custom-control-label" for="slide_4_frame_3">Gunakan Frame Bawah Kiri</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_4_frame_4" <?=$slide_four->frame_4?"checked":""?> name="slide_4_frame_4">
                  <label class="custom-control-label" for="slide_4_frame_4">Gunakan Frame Bawah Kanan</label>
                </div>
              </div>
            </div>
            <br/>
            <div class="row">
              <div class="col-md-4" style="text-align: center;">
                <h6 class="heading-small text-muted mb-4">Foto Lokasi Pernikahan</h6>
                <img src="<?=$slide_four->gambar_lokasi==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_four->gambar_lokasi?>" style="width: 200px; height: 200px; margin: auto; border:1px solid #ccc" id="foto_slide_four_lokasi">
                <input type="file" class="form-control" name="foto_slide_four_lokasi" onChange="readURL(this, '#foto_slide_four_lokasi')" style="width:200px; margin: auto;">
                <i class="note-form">Rekomendasi ukuran 283 x 283 pixel</i>
              </div>
            </div>
            <br/>
            <button type="button" id="btnSaveSlide4" class="btn btn-primary">Save Slide 4</button>
            <a class="btn btn-success text-white" href="<?=base_url()?>#4" target="_blank">Preview</a>
          </form>
        </div>
      </div>
    </div>
  </div>
  <br/>
  <div class="row">
    <div class="col-md-12 mb-5 mb-xl-0">
      <div class="card bg-secondary shadow">
        <div class="card-header bg-transparent">
          <div class="row align-items-center">
            <div class="col">
              <h2 class="mb-0">Slide 5</h2>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form action="#" id="formSlide5" class="form-horizontal">
            <div class="row">
              <div class="col-md-12">
                <h6 class="heading-small text-muted mb-4">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="slide_5_is_bg" <?=$slide_five->is_bg?"checked":""?> name="slide_5_is_bg">
                    <label class="custom-control-label" for="slide_5_is_bg">Gunakan Background</label>
                  </div>
                </h6>

                <img src="<?=$slide_five->background==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_five->background?>" style="width: 100%; height: 335px; margin: auto; border:1px solid #ccc; object-fit: cover" id="foto_slide_five_bg">
                <input type="file" class="form-control" name="foto_slide_five_bg" onChange="readURL(this, '#foto_slide_five_bg')">
              </div>
            </div>
            <hr class="my-4">
            <div class="row">
              <div class="col-md-4" style="text-align: center;">
                <img src="<?=$slide_five->frame==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_five->frame?>" style="width: 200px; height: 200px; margin: auto; border:1px solid #ccc" id="foto_slide_five_frame">
                <input type="file" class="form-control" name="foto_slide_five_frame" onChange="readURL(this, '#foto_slide_five_frame')" style="width:200px; margin: auto;">
                <i class="note-form">Rekomendasi ukuran 283 x 283 pixel</i>
              </div>
              <div class="col-md-8">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_5_frame_1" <?=$slide_five->frame_1?"checked":""?> name="slide_5_frame_1">
                  <label class="custom-control-label" for="slide_5_frame_1">Gunakan Frame Atas Kiri</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_5_frame_2" <?=$slide_five->frame_2?"checked":""?> name="slide_5_frame_2">
                  <label class="custom-control-label" for="slide_5_frame_2">Gunakan Frame Atas Kanan</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_5_frame_3" <?=$slide_five->frame_3?"checked":""?> name="slide_5_frame_3">
                  <label class="custom-control-label" for="slide_5_frame_3">Gunakan Frame Bawah Kiri</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_5_frame_4" <?=$slide_five->frame_4?"checked":""?> name="slide_5_frame_4">
                  <label class="custom-control-label" for="slide_5_frame_4">Gunakan Frame Bawah Kanan</label>
                </div>
              </div>
            </div>
            <br/>
            <button type="button" id="btnSaveSlide5" class="btn btn-primary">Save Slide 5</button>
            <a class="btn btn-success text-white" href="<?=base_url()?>#5" target="_blank">Preview</a>
          </form>
        </div>
      </div>
    </div>
  </div>
  <br/>
  <div class="row">
    <div class="col-md-12 mb-5 mb-xl-0">
      <div class="card bg-secondary shadow">
        <div class="card-header bg-transparent">
          <div class="row align-items-center">
            <div class="col">
              <h2 class="mb-0">Slide 6</h2>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form action="#" id="formSlide6" class="form-horizontal">
            <h6 class="heading-small text-muted mb-4">Masukan Foto Maksimal 5</h6>
            <div class="row">
              <?php
                foreach ($slide_six as $row) {
              ?>
              <div class="col-md-4">
                <img src="<?=$row->file==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/gallery/'.$row->file?>" style="width: 100%; height: 180px; margin: auto; border:1px solid #ccc;border-bottom: 0px; object-fit: contain;" id="foto_slide_six_<?=$row->id?>">
                <input type="file" class="form-control" name="foto_slide_six_<?=$row->id?>" id="input_slide_six_<?=$row->id?>">
                <br/>
              </div>
              <?php
                }
              ?>

            </div>
            <br/>
            <br/>
            <button type="button" id="btnSaveSlide6" class="btn btn-primary">Save Slide 6</button>
            <a class="btn btn-success text-white" href="<?=base_url()?>#6" target="_blank">Preview</a>
          </form>
        </div>
      </div>
    </div>
  </div>
  <br/>
  <div class="row">
    <div class="col-md-12 mb-5 mb-xl-0">
      <div class="card bg-secondary shadow">
        <div class="card-header bg-transparent">
          <div class="row align-items-center">
            <div class="col">
              <h2 class="mb-0">Slide 7</h2>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form action="#" id="formSlide7" class="form-horizontal">
            <div class="row">
              <div class="col-md-4 offset-md-4" style="text-align: center;">
                <h6 class="heading-small text-muted mb-4">Gambar</h6>
                <img src="<?=$slide_seven->bg_main==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_seven->bg_main?>" style="width: 100%; height: 335px; margin: auto; border:1px solid #ccc; object-fit: cover" id="foto_slide_seven_bg_main">
                <input type="file" class="form-control" name="foto_slide_seven_bg_main" onChange="readURL(this, '#foto_slide_seven_bg_main')">
              </div>
            </div>
            <hr class="my-4">
            <div class="row">
              <div class="col-md-12">
                <h6 class="heading-small text-muted mb-4">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="slide_7_is_bg" <?=$slide_seven->is_bg?"checked":""?> name="slide_7_is_bg">
                    <label class="custom-control-label" for="slide_7_is_bg">Gunakan Background</label>
                  </div>
                </h6>

                <img src="<?=$slide_seven->background==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_seven->background?>" style="width: 100%; height: 335px; margin: auto; border:1px solid #ccc; object-fit: cover" id="foto_slide_seven_bg">
                <input type="file" class="form-control" name="foto_slide_seven_bg" onChange="readURL(this, '#foto_slide_seven_bg')">
              </div>
            </div>
            <hr class="my-4">
            <div class="row">
              <div class="col-md-4" style="text-align: center;">
                <img src="<?=$slide_seven->frame==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_seven->frame?>" style="width: 200px; height: 200px; margin: auto; border:1px solid #ccc" id="foto_slide_seven_frame">
                <input type="file" class="form-control" name="foto_slide_seven_frame" onChange="readURL(this, '#foto_slide_seven_frame')" style="width:200px; margin: auto;">
                <i class="note-form">Rekomendasi ukuran 283 x 283 pixel</i>
              </div>
              <div class="col-md-8">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_7_frame_2" <?=$slide_seven->frame_2?"checked":""?> name="slide_7_frame_2">
                  <label class="custom-control-label" for="slide_7_frame_2">Gunakan Frame Atas Kanan</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_7_frame_4" <?=$slide_seven->frame_4?"checked":""?> name="slide_7_frame_4">
                  <label class="custom-control-label" for="slide_7_frame_4">Gunakan Frame Bawah Kanan</label>
                </div>
              </div>
            </div>
            <br/>
            <button type="button" id="btnSaveSlide7" class="btn btn-primary">Save Slide 7</button>
            <a class="btn btn-success text-white" href="<?=base_url()?>#7" target="_blank">Preview</a>
          </form>
        </div>
      </div>
    </div>
  </div>
  <br/>
  <div class="row">
    <div class="col-md-12 mb-5 mb-xl-0">
      <div class="card bg-secondary shadow">
        <div class="card-header bg-transparent">
          <div class="row align-items-center">
            <div class="col">
              <h2 class="mb-0">Slide 8</h2>
            </div>
          </div>
        </div>
        <div class="card-body">
          <form action="#" id="formSlide8" class="form-horizontal">
            <div class="row">
              <div class="col-md-12">
                <h6 class="heading-small text-muted mb-4">
                  <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="slide_8_is_bg" <?=$slide_eight->is_bg?"checked":""?> name="slide_8_is_bg">
                    <label class="custom-control-label" for="slide_8_is_bg">Gunakan Background</label>
                  </div>
                </h6>

                <img src="<?=$slide_eight->background==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_eight->background?>" style="width: 100%; height: 335px; margin: auto; border:1px solid #ccc; object-fit: cover" id="foto_slide_eight_bg">
                <input type="file" class="form-control" name="foto_slide_eight_bg" onChange="readURL(this, '#foto_slide_eight_bg')">
              </div>
            </div>
            <hr class="my-4">
            <div class="row">
              <div class="col-md-4" style="text-align: center;">
                <img src="<?=$slide_eight->frame==''?'https://www.gravatar.com/avatar/dd9dddfe9c9d8a86ff231e94c2a26aca?s=32&d=identicon&r=PG':base_url().'assets/image/'.$slide_eight->frame?>" style="width: 200px; height: 200px; margin: auto; border:1px solid #ccc" id="foto_slide_eight_frame">
                <input type="file" class="form-control" name="foto_slide_eight_frame" onChange="readURL(this, '#foto_slide_eight_frame')" style="width:200px; margin: auto;">
                <i class="note-form">Rekomendasi ukuran 283 x 283 pixel</i>
              </div>
              <div class="col-md-8">
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_8_frame_1" <?=$slide_eight->frame_1?"checked":""?> name="slide_8_frame_1">
                  <label class="custom-control-label" for="slide_8_frame_1">Gunakan Frame Atas Kiri</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_8_frame_2" <?=$slide_eight->frame_2?"checked":""?> name="slide_8_frame_2">
                  <label class="custom-control-label" for="slide_8_frame_2">Gunakan Frame Atas Kanan</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_8_frame_3" <?=$slide_eight->frame_3?"checked":""?> name="slide_8_frame_3">
                  <label class="custom-control-label" for="slide_8_frame_3">Gunakan Frame Bawah Kiri</label>
                </div>
                <br/>
                <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input" id="slide_8_frame_4" <?=$slide_eight->frame_4?"checked":""?> name="slide_8_frame_4">
                  <label class="custom-control-label" for="slide_8_frame_4">Gunakan Frame Bawah Kanan</label>
                </div>
              </div>
            </div>
            <br/>
            <button type="button" id="btnSaveSlide8" class="btn btn-primary">Save Slide 8</button>
            <a class="btn btn-success text-white" href="<?=base_url()?>#8" target="_blank">Preview</a>
          </form>
        </div>
      </div>
    </div>
  </div>

<script type="text/javascript">
  function readURL(input, img) {
    console.log(img);
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      
      reader.onload = function(e) {
        $(img).attr('src', e.target.result);
      }
      
      reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
  }

  $("#input_slide_one_logo").change(function() {
    readURL(this, "#foto_slide_one_logo");
  });

  $("#input_slide_one_mempelai").change(function() {
    readURL(this, "#foto_slide_one_mempelai");
  });

  $("#input_slide_one_frame").change(function() {
    readURL(this, "#foto_slide_one_frame");
  });

  $("#input_slide_one_bg").change(function() {
    readURL(this, "#foto_slide_one_bg");
  });

  $("#input_mempelai_pria").change(function() {
    readURL(this, "#foto_mempelai_pria");
  });

  $("#input_mempelai_wanita").change(function() {
    readURL(this, "#foto_mempelai_wanita");
  });

  $("#input_slide_three_content").change(function() {
    readURL(this, "#foto_slide_three_content");
  });

  $("#input_slide_six_1").change(function() {
    readURL(this, "#foto_slide_six_1");
  });

  $("#input_slide_six_2").change(function() {
    readURL(this, "#foto_slide_six_2");
  });

  $("#input_slide_six_3").change(function() {
    readURL(this, "#foto_slide_six_3");
  });

  $("#input_slide_six_4").change(function() {
    readURL(this, "#foto_slide_six_4");
  });

  $("#input_slide_six_4").change(function() {
    readURL(this, "#foto_slide_six_4");
  });

  $("#input_slide_six_5").change(function() {
    readURL(this, "#foto_slide_six_5");
  });

  $("#input_slide_six_6").change(function() {
    readURL(this, "#foto_slide_six_6");
  });

  //slide 1
  $('#btnSaveSlide1').on('click', function() {
    $('#btnSaveSlide1').text('Saving...'); //change button text
    $('#btnSaveSlide1').attr('disabled',true); //set button disable 

    var formData = new FormData($("#formSlide1")[0]);          
    $.ajax({
      url : "<?php echo site_url('admin/tampilan/slide_one')?>",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      enctype: 'multipart/form-data',
      cache:false,
      dataType:'json',
      success: function(data)
      {
        if(data.status) //if success close modal and reload ajax table
        {
          alert('Data Slide 1 Berhasil Disimpan');
        }
        else
        {
          alert('Gagal menyimpan');
        }
        $('#btnSaveSlide1').text('Save Slide 1'); //change button text
        $('#btnSaveSlide1').attr('disabled',false); //set button enable 


      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding / update data');
        $('#btnSaveSlide1').text('Save Slide 1'); //change button text
        $('#btnSaveSlide1').attr('disabled',false); //set button enable 

      }
    });
  });


  //slide 2
  $('#btnSaveSlide2').on('click', function() {
    $('#btnSaveSlide2').text('Saving...'); //change button text
    $('#btnSaveSlide2').attr('disabled',true); //set button disable 

    var formData = new FormData($("#formSlide2")[0]);          
    $.ajax({
      url : "<?php echo site_url('admin/tampilan/slide_two')?>",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      enctype: 'multipart/form-data',
      cache:false,
      dataType:'json',
      success: function(data)
      {
        if(data.status) //if success close modal and reload ajax table
        {
          alert('Data Slide 2 Berhasil Disimpan');
        }
        else
        {
          alert('Gagal menyimpan');
        }
        $('#btnSaveSlide2').text('Save Slide 2'); //change button text
        $('#btnSaveSlide2').attr('disabled',false); //set button enable 


      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding / update data');
        $('#btnSaveSlide2').text('Save Slide 2'); //change button text
        $('#btnSaveSlide2').attr('disabled',false); //set button enable 

      }
    });
  });

  //slide 3
  $('#btnSaveSlide3').on('click', function() {
    $('#btnSaveSlide3').text('Saving...'); //change button text
    $('#btnSaveSlide3').attr('disabled',true); //set button disable 

    var formData = new FormData($("#formSlide3")[0]);          
    $.ajax({
      url : "<?php echo site_url('admin/tampilan/slide_three')?>",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      enctype: 'multipart/form-data',
      cache:false,
      dataType:'json',
      success: function(data)
      {
        if(data.status) //if success close modal and reload ajax table
        {
          alert('Data Slide 3 Berhasil Disimpan');
        }
        else
        {
          alert('Gagal menyimpan');
        }
        $('#btnSaveSlide3').text('Save Slide 3'); //change button text
        $('#btnSaveSlide3').attr('disabled',false); //set button enable 


      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding / update data');
        $('#btnSaveSlide3').text('Save Slide 3'); //change button text
        $('#btnSaveSlide3').attr('disabled',false); //set button enable 

      }
    });
  });

  //slide 4
  $('#btnSaveSlide4').on('click', function() {
    $('#btnSaveSlide4').text('Saving...'); //change button text
    $('#btnSaveSlide4').attr('disabled',true); //set button disable 

    var formData = new FormData($("#formSlide4")[0]);          
    $.ajax({
      url : "<?php echo site_url('admin/tampilan/slide_four')?>",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      enctype: 'multipart/form-data',
      cache:false,
      dataType:'json',
      success: function(data)
      {
        if(data.status) //if success close modal and reload ajax table
        {
          alert('Data Slide 4 Berhasil Disimpan');
        }
        else
        {
          alert('Gagal menyimpan');
        }
        $('#btnSaveSlide4').text('Save Slide 4'); //change button text
        $('#btnSaveSlide4').attr('disabled',false); //set button enable 


      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding / update data');
        $('#btnSaveSlide4').text('Save Slide 4'); //change button text
        $('#btnSaveSlide4').attr('disabled',false); //set button enable 

      }
    });
  });

  //slide 5
  $('#btnSaveSlide5').on('click', function() {
    $('#btnSaveSlide5').text('Saving...'); //change button text
    $('#btnSaveSlide5').attr('disabled',true); //set button disable 

    var formData = new FormData($("#formSlide5")[0]);          
    $.ajax({
      url : "<?php echo site_url('admin/tampilan/slide_five')?>",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      enctype: 'multipart/form-data',
      cache:false,
      dataType:'json',
      success: function(data)
      {
        if(data.status) //if success close modal and reload ajax table
        {
          alert('Data Slide 5 Berhasil Disimpan');
        }
        else
        {
          alert('Gagal menyimpan');
        }
        $('#btnSaveSlide5').text('Save Slide 5'); //change button text
        $('#btnSaveSlide5').attr('disabled',false); //set button enable 


      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding / update data');
        $('#btnSaveSlide5').text('Save Slide 5'); //change button text
        $('#btnSaveSlide5').attr('disabled',false); //set button enable 

      }
    });
  });

  //slide 6
  $('#btnSaveSlide6').on('click', function() {
    $('#btnSaveSlide6').text('Saving...'); //change button text
    $('#btnSaveSlide6').attr('disabled',true); //set button disable 

    var formData = new FormData($("#formSlide6")[0]);

    $.ajax({
      url : "<?php echo site_url('admin/tampilan/slide_six')?>",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      enctype: 'multipart/form-data',
      cache:false,
      dataType:'json',
      success: function(data)
      {
        if(data.status) //if success close modal and reload ajax table
        {
          alert('Data Slide 6 Berhasil Disimpan');
        }
        else
        {
          alert('Gagal menyimpan');
        }
        $('#btnSaveSlide6').text('Save Slide 6'); //change button text
        $('#btnSaveSlide6').attr('disabled',false); //set button enable 


      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding / update data');
        $('#btnSaveSlide6').text('Save Slide 6'); //change button text
        $('#btnSaveSlide6').attr('disabled',false); //set button enable 

      }
    });
  });

  //slide 7
  $('#btnSaveSlide7').on('click', function() {
    $('#btnSaveSlide7').text('Saving...'); //change button text
    $('#btnSaveSlide7').attr('disabled',true); //set button disable 

    var formData = new FormData($("#formSlide7")[0]);          
    $.ajax({
      url : "<?php echo site_url('admin/tampilan/slide_seven')?>",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      enctype: 'multipart/form-data',
      cache:false,
      dataType:'json',
      success: function(data)
      {
        if(data.status) //if success close modal and reload ajax table
        {
          alert('Data Slide 7 Berhasil Disimpan');
        }
        else
        {
          alert('Gagal menyimpan');
        }
        $('#btnSaveSlide7').text('Save Slide 7'); //change button text
        $('#btnSaveSlide7').attr('disabled',false); //set button enable 


      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding / update data');
        $('#btnSaveSlide7').text('Save Slide 7'); //change button text
        $('#btnSaveSlide7').attr('disabled',false); //set button enable 

      }
    });
  });

  //slide 8
  $('#btnSaveSlide8').on('click', function() {
    $('#btnSaveSlide8').text('Saving...'); //change button text
    $('#btnSaveSlide8').attr('disabled',true); //set button disable 

    var formData = new FormData($("#formSlide8")[0]);          
    $.ajax({
      url : "<?php echo site_url('admin/tampilan/slide_eight')?>",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      enctype: 'multipart/form-data',
      cache:false,
      dataType:'json',
      success: function(data)
      {
        if(data.status) //if success close modal and reload ajax table
        {
          alert('Data Slide 8 Berhasil Disimpan');
        }
        else
        {
          alert('Gagal menyimpan');
        }
        $('#btnSaveSlide8').text('Save Slide 8'); //change button text
        $('#btnSaveSlide8').attr('disabled',false); //set button enable 


      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding / update data');
        $('#btnSaveSlide8').text('Save Slide 8'); //change button text
        $('#btnSaveSlide8').attr('disabled',false); //set button enable 

      }
    });
  });

  //cover
  $('#btnSaveCover').on('click', function() {
    $('#btnSaveCover').text('Saving...'); //change button text
    $('#btnSaveCover').attr('disabled',true); //set button disable 

    var formData = new FormData($("#formCover")[0]);          
    $.ajax({
      url : "<?php echo site_url('admin/tampilan/cover')?>",
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      enctype: 'multipart/form-data',
      cache:false,
      dataType:'json',
      success: function(data)
      {
        if(data.status) //if success close modal and reload ajax table
        {
          alert('Data Cover Berhasil Disimpan');
        }
        else
        {
          alert('Gagal menyimpan');
        }
        $('#btnSaveCover').text('Save Cover'); //change button text
        $('#btnSaveCover').attr('disabled',false); //set button enable 


      },
      error: function (jqXHR, textStatus, errorThrown)
      {
        alert('Error adding / update data');
        $('#btnSaveCover').text('Save Cover'); //change button text
        $('#btnSaveCover').attr('disabled',false); //set button enable 

      }
    });
  });
</script>