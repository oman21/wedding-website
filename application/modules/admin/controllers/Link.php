<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Link extends MX_Controller {

	function __construct() {
        parent::__construct();
		 $this->load->model('dashboard_model','dashboard');
		 is_login();
    }

	public function index()
	{
		$data = array(
			'page' => 'link',
			'title' => 'Generate Link'
		);

		$this->load->view('layout/header', $data);
		$this->load->view('link');
		$this->load->view('layout/footer');
	}
}