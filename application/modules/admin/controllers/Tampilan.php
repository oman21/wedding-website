<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tampilan extends MX_Controller {

	function __construct() {
    parent::__construct();
    is_login();
  }

	public function index()
	{
		$this->db->where('id', 1);
		$data_nikah = $this->db->get('data_nikah')->row();

		$this->db->where('id', 1);
		$cover = $this->db->get('cover')->row();

		$this->db->where('id', 1);
		$slide_one = $this->db->get('slide_one')->row();

		$this->db->where('id', 1);
		$slide_two = $this->db->get('slide_two')->row();

		$this->db->where('id', 1);
		$slide_three = $this->db->get('slide_three')->row();

		$this->db->where('id', 1);
		$slide_four = $this->db->get('slide_four')->row();

		$this->db->where('id', 1);
		$slide_five = $this->db->get('slide_five')->row();

		$slide_six = $this->db->get('slide_six')->result();

		$this->db->where('id', 1);
		$slide_seven = $this->db->get('slide_seven')->row();

		$this->db->where('id', 1);
		$slide_eight = $this->db->get('slide_eight')->row();

		$data = array(
			'page' => 'tampilan',
			'title' => 'Tampilan',
			'data_nikah' => $data_nikah,
			'slide_two' => $slide_two,
			'slide_one' => $slide_one,
			'slide_three' => $slide_three,
			'slide_four' => $slide_four,
			'slide_five' => $slide_five,
			'slide_six' => $slide_six,
			'slide_seven' => $slide_seven,
			'slide_eight' => $slide_eight,
			'cover' => $cover
		);

		$this->load->view('layout/header', $data);
		$this->load->view('tampilan', $data);
		$this->load->view('layout/footer');
	}

	public function slide_one()
	{
		$data = array();

    $tmpFilePath1 = $_FILES['foto_slide_one_logo']['tmp_name'];
    if ($tmpFilePath1 != "") {
        $gbr1 = $_FILES['foto_slide_one_logo']['name'];
        if (move_uploaded_file($tmpFilePath1, "assets/image/".$gbr1)) {
            $data['logo'] = $gbr1;
        }
    }

    $tmpFilePath2 = $_FILES['foto_slide_one_mempelai']['tmp_name'];
    if ($tmpFilePath2 != "") {
        $gbr2 = $_FILES['foto_slide_one_mempelai']['name'];
        if (move_uploaded_file($tmpFilePath2, "assets/image/".$gbr2)) {
            $data['foto_mempelai'] = $gbr2;
        }
    }

    $tmpFilePathBg = $_FILES['foto_slide_one_bg']['tmp_name'];
    if ($tmpFilePathBg != "") {
        $bg = $_FILES['foto_slide_one_bg']['name'];
        if (move_uploaded_file($tmpFilePathBg, "assets/image/".$bg)) {
            $data['background'] = $bg;
        }
    }

    $tmpFilePathFrame = $_FILES['foto_slide_one_frame']['tmp_name'];
    if ($tmpFilePathFrame != "") {
        $frame = $_FILES['foto_slide_one_frame']['name'];
        if (move_uploaded_file($tmpFilePathFrame, "assets/image/".$frame)) {
            $data['frame'] = $frame;
        }
    }

    $data['is_bg'] = $this->input->post('slide_1_frame_is_bg')?1:0;
    $data['frame_1'] = $this->input->post('slide_1_frame_1')?1:0;
    $data['frame_2'] = $this->input->post('slide_1_frame_2')?1:0;
    $data['frame_3'] = $this->input->post('slide_1_frame_3')?1:0;
    $data['frame_4'] = $this->input->post('slide_1_frame_4')?1:0;

    $this->db->where('id', 1);
		$update = $this->db->update('slide_one', $data);

		if($update){
			echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}	
	}

	public function slide_two()
	{
		$data = array();

    $tmpFilePath1 = $_FILES['foto_mempelai_pria']['tmp_name'];
    if ($tmpFilePath1 != "") {
        $gbr1 = $_FILES['foto_mempelai_pria']['name'];
        if (move_uploaded_file($tmpFilePath1, "assets/image/".$gbr1)) {
            $data['foto_mempelai_pria'] = $gbr1;
        }
    }

    $tmpFilePath2 = $_FILES['foto_mempelai_wanita']['tmp_name'];
    if ($tmpFilePath2 != "") {
        $gbr2 = $_FILES['foto_mempelai_wanita']['name'];
        if (move_uploaded_file($tmpFilePath2, "assets/image/".$gbr2)) {
            $data['foto_mempelai_wanita'] = $gbr2;
        }
    }

    $tmpFilePathBg = $_FILES['foto_slide_two_bg']['tmp_name'];
    if ($tmpFilePathBg != "") {
        $bg = $_FILES['foto_slide_two_bg']['name'];
        if (move_uploaded_file($tmpFilePathBg, "assets/image/".$bg)) {
            $data['background'] = $bg;
        }
    }

    $tmpFilePathFrame = $_FILES['foto_slide_two_frame']['tmp_name'];
    if ($tmpFilePathFrame != "") {
        $frame = $_FILES['foto_slide_two_frame']['name'];
        if (move_uploaded_file($tmpFilePathFrame, "assets/image/".$frame)) {
            $data['frame'] = $frame;
        }
    }

    $data['is_bg'] = $this->input->post('slide_2_is_bg')?1:0;
    $data['frame_1'] = $this->input->post('slide_2_frame_1')?1:0;
    $data['frame_2'] = $this->input->post('slide_2_frame_2')?1:0;
    $data['frame_3'] = $this->input->post('slide_2_frame_3')?1:0;
    $data['frame_4'] = $this->input->post('slide_2_frame_4')?1:0;

    $this->db->where('id', 1);
		$update = $this->db->update('slide_two', $data);

		if($update){
			echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function slide_three()
	{
		$data = array();

    $tmpFilePath1 = $_FILES['foto_slide_three_content']['tmp_name'];
    if ($tmpFilePath1 != "") {
        $gbr1 = $_FILES['foto_slide_three_content']['name'];
        if (move_uploaded_file($tmpFilePath1, "assets/image/".$gbr1)) {
            $data['content'] = $gbr1;
        }
    }

    $tmpFilePathBg = $_FILES['foto_slide_three_bg']['tmp_name'];
    if ($tmpFilePathBg != "") {
        $bg = $_FILES['foto_slide_three_bg']['name'];
        if (move_uploaded_file($tmpFilePathBg, "assets/image/".$bg)) {
            $data['background'] = $bg;
        }
    }

    $tmpFilePathFrame = $_FILES['foto_slide_three_frame']['tmp_name'];
    if ($tmpFilePathFrame != "") {
        $frame = $_FILES['foto_slide_three_frame']['name'];
        if (move_uploaded_file($tmpFilePathFrame, "assets/image/".$frame)) {
            $data['frame'] = $frame;
        }
    }

    $data['is_bg'] = $this->input->post('slide_3_is_bg')?1:0;
    $data['frame_1'] = $this->input->post('slide_3_frame_1')?1:0;
    $data['frame_2'] = $this->input->post('slide_3_frame_2')?1:0;
    $data['frame_3'] = $this->input->post('slide_3_frame_3')?1:0;
    $data['frame_4'] = $this->input->post('slide_3_frame_4')?1:0;

    $this->db->where('id', 1);
		$update = $this->db->update('slide_three', $data);

		if($update){
			echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function slide_four()
	{
		$data = array();

    $tmpFilePathBg = $_FILES['foto_slide_four_bg']['tmp_name'];
    if ($tmpFilePathBg != "") {
        $bg = $_FILES['foto_slide_four_bg']['name'];
        if (move_uploaded_file($tmpFilePathBg, "assets/image/".$bg)) {
            $data['background'] = $bg;
        }
    }

    $tmpFilePathFrame = $_FILES['foto_slide_four_frame']['tmp_name'];
    if ($tmpFilePathFrame != "") {
        $frame = $_FILES['foto_slide_four_frame']['name'];
        if (move_uploaded_file($tmpFilePathFrame, "assets/image/".$frame)) {
            $data['frame'] = $frame;
        }
    }

    $tmpFilePathLokasi = $_FILES['foto_slide_four_lokasi']['tmp_name'];
    if ($tmpFilePathLokasi != "") {
        $lokasi = $_FILES['foto_slide_four_lokasi']['name'];
        if (move_uploaded_file($tmpFilePathLokasi, "assets/image/".$lokasi)) {
            $data['gambar_lokasi'] = $lokasi;
        }
    }

    $data['is_bg'] = $this->input->post('slide_4_is_bg')?1:0;
    $data['frame_1'] = $this->input->post('slide_4_frame_1')?1:0;
    $data['frame_2'] = $this->input->post('slide_4_frame_2')?1:0;
    $data['frame_3'] = $this->input->post('slide_4_frame_3')?1:0;
    $data['frame_4'] = $this->input->post('slide_4_frame_4')?1:0;

    $this->db->where('id', 1);
		$update = $this->db->update('slide_four', $data);

		if($update){
			echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function slide_five()
	{
		$data = array();

    $tmpFilePathBg = $_FILES['foto_slide_five_bg']['tmp_name'];
    if ($tmpFilePathBg != "") {
        $bg = $_FILES['foto_slide_five_bg']['name'];
        if (move_uploaded_file($tmpFilePathBg, "assets/image/".$bg)) {
            $data['background'] = $bg;
        }
    }

    $tmpFilePathFrame = $_FILES['foto_slide_five_frame']['tmp_name'];
    if ($tmpFilePathFrame != "") {
        $frame = $_FILES['foto_slide_five_frame']['name'];
        if (move_uploaded_file($tmpFilePathFrame, "assets/image/".$frame)) {
            $data['frame'] = $frame;
        }
    }

    $data['is_bg'] = $this->input->post('slide_5_is_bg')?1:0;
    $data['frame_1'] = $this->input->post('slide_5_frame_1')?1:0;
    $data['frame_2'] = $this->input->post('slide_5_frame_2')?1:0;
    $data['frame_3'] = $this->input->post('slide_5_frame_3')?1:0;
    $data['frame_4'] = $this->input->post('slide_5_frame_4')?1:0;

    $this->db->where('id', 1);
		$update = $this->db->update('slide_five', $data);

		if($update){
			echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function slide_six(){
		$this->db->trans_start();

    $tmpFilePath1 = $_FILES['foto_slide_six_1']['tmp_name'];
    if ($tmpFilePath1 != "") {
        $gbr1 = $_FILES['foto_slide_six_1']['name'];
        if (move_uploaded_file($tmpFilePath1, "assets/image/gallery/".$gbr1)) {
         	$this->db->where('id', 1);
         	$this->db->update('slide_six', array('file' => $gbr1));
        }
    }else{
    	$this->db->where('id', 1);
      $this->db->update('slide_six', array('file' => ""));
    }

    $tmpFilePath2 = $_FILES['foto_slide_six_2']['tmp_name'];
    if ($tmpFilePath2 != "") {
        $gbr2 = $_FILES['foto_slide_six_2']['name'];
        if (move_uploaded_file($tmpFilePath2, "assets/image/gallery/".$gbr2)) {
         	$this->db->where('id', 2);
         	$this->db->update('slide_six', array('file' => $gbr2));
        }
    }else{
    	$this->db->where('id', 2);
      $this->db->update('slide_six', array('file' => ""));
    }

    $tmpFilePath3 = $_FILES['foto_slide_six_3']['tmp_name'];
    if ($tmpFilePath3 != "") {
        $gbr3 = $_FILES['foto_slide_six_3']['name'];
        if (move_uploaded_file($tmpFilePath3, "assets/image/gallery/".$gbr3)) {
         	$this->db->where('id', 3);
         	$this->db->update('slide_six', array('file' => $gbr3));
        }
    }else{
    	$this->db->where('id', 3);
      $this->db->update('slide_six', array('file' => ""));
    }

    $tmpFilePath4 = $_FILES['foto_slide_six_4']['tmp_name'];
    if ($tmpFilePath4 != "") {
        $gbr4 = $_FILES['foto_slide_six_4']['name'];
        if (move_uploaded_file($tmpFilePath4, "assets/image/gallery/".$gbr4)) {
         	$this->db->where('id', 4);
         	$this->db->update('slide_six', array('file' => $gbr4));
        }
    }else{
    	$this->db->where('id', 4);
      $this->db->update('slide_six', array('file' => ""));
    }

    $tmpFilePath5 = $_FILES['foto_slide_six_5']['tmp_name'];
    if ($tmpFilePath5 != "") {
        $gbr5 = $_FILES['foto_slide_six_5']['name'];
        if (move_uploaded_file($tmpFilePath5, "assets/image/gallery/".$gbr5)) {
         	$this->db->where('id', 5);
         	$this->db->update('slide_six', array('file' => $gbr5));
        }
    }else{
    	$this->db->where('id', 5);
      $this->db->update('slide_six', array('file' => ""));
    }

    $update = $this->db->trans_complete();

    if($update){
    	echo json_encode(array("status" => TRUE));
    }else{
    	echo json_encode(array("status" => FALSE));
    }
	}

	public function slide_seven()
	{
		$data = array();

		$tmpFilePathBgMain = $_FILES['foto_slide_seven_bg_main']['tmp_name'];
    if ($tmpFilePathBgMain != "") {
        $bgMain = $_FILES['foto_slide_seven_bg_main']['name'];
        if (move_uploaded_file($tmpFilePathBgMain, "assets/image/".$bgMain)) {
            $data['bg_main'] = $bgMain;
        }
    }

    $tmpFilePathBg = $_FILES['foto_slide_seven_bg']['tmp_name'];
    if ($tmpFilePathBg != "") {
        $bg = $_FILES['foto_slide_seven_bg']['name'];
        if (move_uploaded_file($tmpFilePathBg, "assets/image/".$bg)) {
            $data['background'] = $bg;
        }
    }

    $tmpFilePathFrame = $_FILES['foto_slide_seven_frame']['tmp_name'];
    if ($tmpFilePathFrame != "") {
        $frame = $_FILES['foto_slide_seven_frame']['name'];
        if (move_uploaded_file($tmpFilePathFrame, "assets/image/".$frame)) {
            $data['frame'] = $frame;
        }
    }

    $data['is_bg'] = $this->input->post('slide_7_is_bg')?1:0;
    $data['frame_2'] = $this->input->post('slide_7_frame_2')?1:0;
    $data['frame_4'] = $this->input->post('slide_7_frame_4')?1:0;

    $this->db->where('id', 1);
		$update = $this->db->update('slide_seven', $data);

		if($update){
			echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function slide_eight()
	{
		$data = array();

    $tmpFilePathBg = $_FILES['foto_slide_eight_bg']['tmp_name'];
    if ($tmpFilePathBg != "") {
        $bg = $_FILES['foto_slide_eight_bg']['name'];
        if (move_uploaded_file($tmpFilePathBg, "assets/image/".$bg)) {
            $data['background'] = $bg;
        }
    }

    $tmpFilePathFrame = $_FILES['foto_slide_eight_frame']['tmp_name'];
    if ($tmpFilePathFrame != "") {
        $frame = $_FILES['foto_slide_eight_frame']['name'];
        if (move_uploaded_file($tmpFilePathFrame, "assets/image/".$frame)) {
            $data['frame'] = $frame;
        }
    }

    $data['is_bg'] = $this->input->post('slide_8_is_bg')?1:0;
    $data['frame_1'] = $this->input->post('slide_8_frame_1')?1:0;
    $data['frame_2'] = $this->input->post('slide_8_frame_2')?1:0;
    $data['frame_3'] = $this->input->post('slide_8_frame_3')?1:0;
    $data['frame_4'] = $this->input->post('slide_8_frame_4')?1:0;

    $this->db->where('id', 1);
		$update = $this->db->update('slide_eight', $data);

		if($update){
			echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}

	public function cover()
	{
		$data = array();

		$tmpFilePath1 = $_FILES['foto_cover_logo']['tmp_name'];
    if ($tmpFilePath1 != "") {
        $gbr1 = $_FILES['foto_cover_logo']['name'];
        if (move_uploaded_file($tmpFilePath1, "assets/image/".$gbr1)) {
            $data['logo'] = $gbr1;
        }
    }

    $tmpFilePathBg = $_FILES['foto_cover_bg']['tmp_name'];
    if ($tmpFilePathBg != "") {
        $bg = $_FILES['foto_cover_bg']['name'];
        if (move_uploaded_file($tmpFilePathBg, "assets/image/".$bg)) {
            $data['background'] = $bg;
        }
    }

    $tmpFilePathFrame = $_FILES['foto_cover_frame']['tmp_name'];
    if ($tmpFilePathFrame != "") {
        $frame = $_FILES['foto_cover_frame']['name'];
        if (move_uploaded_file($tmpFilePathFrame, "assets/image/".$frame)) {
            $data['frame'] = $frame;
        }
    }

    $data['is_bg'] = $this->input->post('cover_is_bg')?1:0;
    $data['frame_1'] = $this->input->post('cover_frame_1')?1:0;
    $data['frame_2'] = $this->input->post('cover_frame_2')?1:0;
    $data['frame_3'] = $this->input->post('cover_frame_3')?1:0;
    $data['frame_4'] = $this->input->post('cover_frame_4')?1:0;

    $this->db->where('id', 1);
		$update = $this->db->update('cover', $data);

		if($update){
			echo json_encode(array("status" => TRUE));
		}else{
			echo json_encode(array("status" => FALSE));
		}
	}
}
