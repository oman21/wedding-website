<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MX_Controller {

	function __construct() {
        parent::__construct();
		 $this->load->model('dashboard_model','dashboard');
		 is_login();
    }

	public function index()
	{
		$data = array(
			'page' => 'dashboard',
			'title' => 'Ucapan & Doa'
		);

		$this->load->view('layout/header', $data);
		$this->load->view('dashboard');
		$this->load->view('layout/footer');
	}

	public function ajax_list()
    {
        $list = $this->dashboard->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $undangan) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $undangan->nama;
            $row[] = $undangan->ucapan;
            $row[] = $undangan->kehadiran;
            $row[] = date_format(date_create($undangan->created_date), 'd-m-Y H:i:s');
 
            $data[] = $row;
        }
 
        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->dashboard->count_all(),
                        "recordsFiltered" => $this->dashboard->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }
}
