<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_nikah extends MX_Controller {

	function __construct() {
        parent::__construct();
        is_login();
    }
	public function index()
	{	
		$this->db->where('id', 1);
		$data_nikah = $this->db->get('data_nikah')->row();

		$tamu_undangan = $this->db->get('tamu_undangan')->result();

		$data = array(
			'page' => 'data_nikah',
			'title' => 'Data Nikah',
			'data_nikah' => $data_nikah,
			'tamu_undangan' => $tamu_undangan
		);

		$this->load->view('layout/header', $data);
		$this->load->view('data_nikah');
		$this->load->view('layout/footer');
	}

	public function save()
	{

		$this->_validate();
        $data = array(
	        'mempelai_pria' => $this->input->post('mempelai_pria'),
	        'mempelai_wanita' => $this->input->post('mempelai_wanita'),
	        'ayah_pria' => $this->input->post('ayah_pria'),
	        'ibu_pria' => $this->input->post('ibu_pria'),
	        'ayah_wanita' => $this->input->post('ayah_wanita'),
	        'ibu_wanita' => $this->input->post('ibu_wanita'),
	        'tanggal_akad' => date_format(date_create($this->input->post('tanggal_akad').' '.$this->input->post('jam_akad').':00'), 'Y-m-d H:i:s'),
	        'alamat_akad' => $this->input->post('alamat_akad'),
	        'tanggal_resepsi' => date_format(date_create($this->input->post('tanggal_resepsi').' '.$this->input->post('jam_resepsi').':00'), 'Y-m-d H:i:s'),
	        'alamat_resepsi' => $this->input->post('alamat_resepsi'),
	        'url_map' => $this->input->post('url_map'),
	        'putra_ke' => $this->input->post('putra_ke'),
	        'putri_ke' => $this->input->post('putri_ke'),
	        'tempat_akad' => $this->input->post('tempat_akad'),
	        'tempat_resepsi' => $this->input->post('tempat_resepsi'),
        );

        $this->db->trans_start();

        $this->db->where('id', 1);
		$this->db->update('data_nikah', $data);

		$this->db->empty_table('tamu_undangan');

		if($this->input->post('undangan')){
			foreach ($this->input->post('undangan') as $row) {
				$this->db->insert('tamu_undangan', array('nama' => $row));
			}
		}

		$update = $this->db->trans_complete();
        if($update){
        	echo json_encode(array("status" => TRUE));
        }else{
        	echo json_encode(array("status" => FALSE));
        }
	}

	private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('mempelai_pria') == '')
        {
            $data['inputerror'][] = 'mempelai_pria';
            $data['error_string'][] = 'Harus diisi!';
            $data['status'] = FALSE;
        }

        if($this->input->post('mempelai_wanita') == '')
        {
            $data['inputerror'][] = 'mempelai_wanita';
            $data['error_string'][] = 'Harus diisi!';
            $data['status'] = FALSE;
        }

        if($this->input->post('ayah_pria') == '')
        {
            $data['inputerror'][] = 'ayah_pria';
            $data['error_string'][] = 'Harus diisi!';
            $data['status'] = FALSE;
        }

        if($this->input->post('ibu_pria') == '')
        {
            $data['inputerror'][] = 'ibu_pria';
            $data['error_string'][] = 'Harus diisi!';
            $data['status'] = FALSE;
        }

        if($this->input->post('ayah_wanita') == '')
        {
            $data['inputerror'][] = 'ayah_wanita';
            $data['error_string'][] = 'Harus diisi!';
            $data['status'] = FALSE;
        }

        if($this->input->post('ibu_wanita') == '')
        {
            $data['inputerror'][] = 'ibu_wanita';
            $data['error_string'][] = 'Harus diisi!';
            $data['status'] = FALSE;
        }

        if($this->input->post('tanggal_akad') == '')
        {
            $data['inputerror'][] = 'tanggal_akad';
            $data['error_string'][] = 'Harus diisi!';
            $data['status'] = FALSE;
        }

        if($this->input->post('jam_akad') == '')
        {
            $data['inputerror'][] = 'jam_akad';
            $data['error_string'][] = 'Harus diisi!';
            $data['status'] = FALSE;
        }

        if($this->input->post('alamat_akad') == '')
        {
            $data['inputerror'][] = 'alamat_akad';
            $data['error_string'][] = 'Harus diisi!';
            $data['status'] = FALSE;
        }

        if($this->input->post('tanggal_resepsi') == '')
        {
            $data['inputerror'][] = 'tanggal_resepsi';
            $data['error_string'][] = 'Harus diisi!';
            $data['status'] = FALSE;
        }

        if($this->input->post('jam_resepsi') == '')
        {
            $data['inputerror'][] = 'jam_resepsi';
            $data['error_string'][] = 'Harus diisi!';
            $data['status'] = FALSE;
        }

        if($this->input->post('alamat_resepsi') == '')
        {
            $data['inputerror'][] = 'alamat_resepsi';
            $data['error_string'][] = 'Harus diisi!';
            $data['status'] = FALSE;
        }

        if($this->input->post('url_map') == '')
        {
            $data['inputerror'][] = 'url_map';
            $data['error_string'][] = 'Harus diisi!';
            $data['status'] = FALSE;
        }
 
        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
}