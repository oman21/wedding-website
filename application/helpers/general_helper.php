<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if(!function_exists('is_login'))
{
	function is_login(){
		$ci = &get_instance();
		if (empty($ci->session->userdata('user_id'))) {
			redirect(base_url('auth/login'));
		}
	}
}